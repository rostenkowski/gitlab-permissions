package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/application"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/collect"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/config"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/store"
)

const shutDownTimeout = 10 * time.Second

func main() {
	// Read configuration
	cfg, err := config.ParseConfig(os.Args[1:])
	if err != nil {
		log.Fatalf("failed to parse cfg: %v", err)
	}

	// Dependency injection
	c, err := collect.NewGitLabClient(cfg.AccessToken, cfg.CacheDir, cfg.CacheTTL, &collect.CustomTransportFactory{})
	if err != nil {
		log.Fatalf("failed to create gitlab c: %s", err)
	}

	collector, err := collect.NewGitlabCollector(&cfg.GroupID, c.Groups, c.ProjectMembers)
	if err != nil {
		log.Fatalf("failed to initialize gitlab collector: %v", err)
	}

	storage := store.NewMemoryStorage(false, domain.Collection{})

	app := application.NewApp(cfg, collector, storage, time.Duration(cfg.CacheTTL)*time.Second)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())

	go app.Init(ctx)

	go app.Watch(ctx, &cfg.CacheDir)

	go app.Run()

	<-quit   // Wait for the exit signal
	cancel() // Propagate the cancel event to all contexts-aware operations like Init or Watch
	log.Println("shut-down signal received...")

	// Create a deadline to shut down the HTTP server
	ctxShutdown, cancelShutdown := context.WithTimeout(ctx, shutDownTimeout)
	defer cancelShutdown()

	// Stop HTTP server
	go func() {
		if err := app.Server.Shutdown(ctxShutdown); err != nil {
			log.Printf("server failed to shut-down: %s\n", err)
		}
	}()

	// Stop HTTPS server if enabled
	if cfg.EnableSSL {
		if err := app.SSLServer.Shutdown(ctxShutdown); err != nil {
			log.Printf("server failed to shut-down: %s\n", err)
		}
	}

	log.Println("shut-down complete, exiting now...")
}
