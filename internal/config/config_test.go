package config

import (
	"crypto/tls"
	"errors"
	"flag"
	"log"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/filesystem"
)

// TestParseConfig uses table-driven tests to validate configuration parsing.
func TestParseConfig(t *testing.T) {
	tests := []struct {
		name             string
		cmdArgs          []string
		envVars          map[string]string
		expectedError    bool
		expectedErrMsg   string
		expectedConfig   *Config
		patchCheckDir    func() // Function to patch filesystem.CheckDir
		patchLoadKeyPair func() // Function to patch filesystem.CheckDir
	}{
		{
			name:          "Valid config",
			cmdArgs:       []string{"-port", "8081", "-sslPort", "8082", "-cacheDir", "/tmp/cache", "-cacheTTL", "1200"},
			envVars:       map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42", "API_KEY": "woodoo"},
			expectedError: false,
			expectedConfig: &Config{
				Port:        8081,
				SSLPort:     8082,
				CacheDir:    "/tmp/cache",
				CacheTTL:    1200,
				AccessToken: "token123",
				GroupID:     42,
				EnableSSL:   false,
				CertFile:    "",
				KeyFile:     "",
				AuthToken:   "woodoo",
			},
		},
		{
			name:          "Valid config, SSL enabled",
			cmdArgs:       []string{"-ssl", "-sslPort", "8444", "-cert", "/tmp/cert.pem", "-key", "/tmp/key.pem", "-port", "8081", "-cacheDir", "/tmp/cache", "-cacheTTL", "1200"},
			envVars:       map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError: false,
			expectedConfig: &Config{
				Port:        8081,
				SSLPort:     8444,
				CacheDir:    "/tmp/cache",
				CacheTTL:    1200,
				AccessToken: "token123",
				GroupID:     42,
				EnableSSL:   true,
				CertFile:    "/tmp/cert.pem",
				KeyFile:     "/tmp/key.pem",
			},
			patchLoadKeyPair: func() {
				orig := loadCert
				loadCert = func(certFile, keyFile string) (tls.Certificate, error) {
					_ = certFile
					_ = keyFile

					return tls.Certificate{}, nil
				}
				t.Cleanup(func() {
					loadCert = orig
				})
			},
		},
		{
			name:           "Valid config, SSL enabled, invalid cert",
			cmdArgs:        []string{"-ssl", "-cert", "/tmp/cert.pem", "-key", "/tmp/key.pem", "-port", "8081", "-cacheDir", "/tmp/cache", "-cacheTTL", "1200"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "failed to load TLS certificate: failed to load certificate and key: scrumpfgh",
			patchLoadKeyPair: func() {
				orig := loadCert
				loadCert = func(certFile, keyFile string) (tls.Certificate, error) {
					_ = certFile
					_ = keyFile

					return tls.Certificate{}, errors.New("scrumpfgh")
				}
				t.Cleanup(func() {
					loadCert = orig
				})
			},
		},
		{
			name:           "Invalid port",
			cmdArgs:        []string{"-port", "70000", "-cacheDir", "/tmp/cache", "-cacheTTL", "1200"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "invalid port number: 70000",
		},
		{
			name:           "0 as group ID",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/tmp/cache", "-cacheTTL", "900"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "0"},
			expectedError:  true,
			expectedErrMsg: "empty group id",
		},
		{
			name:           "Empty string group ID",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/tmp/cache", "-cacheTTL", "900"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": ""},
			expectedError:  true,
			expectedErrMsg: "failed to parse group id: (raw string value: ''), error: strconv.Atoi: parsing \"\": invalid syntax",
		},
		{
			name:           "Invalid group ID",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/tmp/cache", "-cacheTTL", "900"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "not a number"},
			expectedError:  true,
			expectedErrMsg: "failed to parse group id: (raw string value: 'not a number'), error: strconv.Atoi: parsing \"not a number\": invalid syntax",
		},
		{
			name:           "Empty access token",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/tmp/cache", "-cacheTTL", "900"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "empty access token",
		},
		{
			name:           "Invalid cache TTL",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/tmp/cache", "-cacheTTL", "59"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "invalid cacheTTL: 59",
		},
		{
			name:           "Invalid cache directory",
			cmdArgs:        []string{"-port", "8000", "-cacheDir", "/invalid/cache/dir", "-cacheTTL", "900"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "cache directory check failed",
			patchCheckDir: func() {
				originalCheckDir := filesystem.CheckDirFunc
				filesystem.CheckDirFunc = func(_ string) bool {
					return false
				}
				t.Cleanup(func() {
					filesystem.CheckDirFunc = originalCheckDir
				})
			},
		},
		{
			name:           "Flag parsing error",
			cmdArgs:        []string{"-port", "invalid"},
			envVars:        map[string]string{"GITLAB_ACCESS_TOKEN": "token123", "GITLAB_GROUP_ID": "42"},
			expectedError:  true,
			expectedErrMsg: "invalid value \"invalid\" for flag -port",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset flag set for each test
			flag.CommandLine = flag.NewFlagSet(tt.name, flag.ContinueOnError)

			// Apply monkey patch if defined
			if tt.patchCheckDir != nil {
				tt.patchCheckDir()
			}

			if tt.patchLoadKeyPair != nil {
				tt.patchLoadKeyPair()
			}

			// Set up environment variables
			for key, value := range tt.envVars {
				_ = os.Setenv(key, value)
			}

			defer func() {
				for key := range tt.envVars {
					_ = os.Unsetenv(key)
				}
			}()

			// Parse command-line arguments
			config, err := ParseConfig(tt.cmdArgs)

			if (err != nil) != tt.expectedError {
				t.Fatalf("ParseConfig() error = %v, expectedError %v", err, tt.expectedError)
			}

			if err != nil && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("Expected error message to contain '%s', got '%s'", tt.expectedErrMsg, err.Error())
			}

			if err == nil && !reflect.DeepEqual(config, tt.expectedConfig) {
				t.Errorf("Expected config: \n%+v, got: \n%+v", tt.expectedConfig, config)
			}
		})
	}
}

func TestIsValidPort(t *testing.T) {
	tests := []struct {
		name     string
		port     int
		expected bool
	}{
		{"Valid port (low boundary)", 1, true},
		{"Valid port (high boundary)", 65535, true},
		{"Invalid port (below low boundary)", 0, false},
		{"Invalid port (above high boundary)", 65536, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := isValidPort(tt.port)
			assert.Equal(t, tt.expected, result, "IsValidPort(%d) should be %v", tt.port, tt.expected)
		})
	}
}

func TestIsValidTTL(t *testing.T) {
	tests := []struct {
		name     string
		ttl      int
		expected bool
	}{
		{"Valid TTL (low boundary)", 60, true},
		{"Valid TTL (high boundary)", 604800, true},
		{"Invalid TTL (below low boundary)", 59, false},
		{"Invalid TTL (above high boundary)", 604801, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := isValidTTL(tt.ttl)
			assert.Equal(t, tt.expected, result, "IsValidTTL(%d) should be %v", tt.ttl, tt.expected)
		})
	}
}

func TestMaskSecret(t *testing.T) {
	tests := []struct {
		name     string
		secret   string
		expected string
	}{
		{"Password", "l3zD1vsSFbAIWd3", "**********Wd3"},
		{"Token", "g-l3zD1vsSFbAIWd3-l3zD1vsSFbAIWd3", "**********Wd3"},
		{"Short secret", "AIWd3", "**********Wd3"},
		{"Very short secret", "42", "**********"},
		{"Empty secret", "", "**********"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := maskSecret(tt.secret, 3, 12)

			log.Println(tt.secret, "masked as ", result)

			if len(tt.secret) < 4 {
				for _, c := range result {
					assert.Equal(t, "*", string(c), "expected secret to contain only '*' characters, got '%s'", result)
				}
			} else if !strings.HasSuffix(result, tt.expected) {
				t.Errorf("masked secret (%q) should end with '%s'", tt.secret, tt.expected)
			}
		})
	}
}
