package config

import (
	"crypto/tls"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/filesystem"
)

var loadCert = tls.LoadX509KeyPair

// Config holds the configuration settings for the application.
type Config struct {
	Port        int    // Port to listen on
	SSLPort     int    // Port to listen on for SSL connections
	CacheDir    string // Cache directory
	CacheTTL    int    // Time to cache HTTP responses for, in seconds
	AccessToken string // GitLab access token
	GroupID     int    // GitLab group ID
	EnableSSL   bool   // Enable SSL
	KeyFile     string // Private key for SSL
	CertFile    string // Certificate for SSL
	AuthToken   string // Authentication key
}

var errGroupID = errors.New("failed to parse group id")

var defaultPort = 8080
var defaultSSLPort = 8443
var defaultTTL = 900

const minSecretLength = 4
const maskLength = 10

// ParseConfig parses the configuration from command-line flags and environment variables.
func ParseConfig(args []string) (*Config, error) {
	// Parse configuration from command-line flags
	port := flag.Int("port", defaultPort, "The port to listen on")
	sslPort := flag.Int("sslPort", defaultSSLPort, "The port to listen on")
	cacheDir := flag.String("cacheDir", "./cache", "The directory where the HTTP cache files are stored")
	cacheTTL := flag.Int("cacheTTL", defaultTTL, "Time to cache HTTP responses for in seconds")
	enableSSL := flag.Bool("ssl", false, "Enable SSL")
	certFile := flag.String("cert", "cert.pem", "Path to the SSL certificate")
	keyFile := flag.String("key", "key.pem", "Path to the SSL key")

	// Capture flag parsing error
	err := flag.CommandLine.Parse(args)
	if err != nil {
		return nil, fmt.Errorf("failed to parse args: %w", err)
	}

	// Parse configuration from environment variables
	authToken := os.Getenv("API_KEY")
	accessToken := os.Getenv("GITLAB_ACCESS_TOKEN")
	groupIDStr := os.Getenv("GITLAB_GROUP_ID")
	groupID, err := strconv.Atoi(groupIDStr)

	if err != nil {
		return nil, fmt.Errorf("%w: (raw string value: '%s'), error: %w", errGroupID, groupIDStr, err)
	}

	// Validate configuration
	if accessToken == "" {
		return nil, errors.New("empty access token")
	}

	if groupID == 0 {
		return nil, errors.New("empty group id")
	}

	if !isValidPort(*port) {
		return nil, fmt.Errorf("invalid port number: %d", *port)
	}

	if !isValidTTL(*cacheTTL) {
		return nil, fmt.Errorf("invalid cacheTTL: %d", *cacheTTL)
	}

	// Cache directory validation
	if ok := filesystem.CheckDirFunc(*cacheDir); !ok {
		return nil, fmt.Errorf("cache directory check failed: %w", err)
	}

	cfg := &Config{
		Port:        *port,
		CacheDir:    *cacheDir,
		CacheTTL:    *cacheTTL,
		AccessToken: accessToken,
		GroupID:     groupID,
		EnableSSL:   *enableSSL,
		SSLPort:     *sslPort,
		AuthToken:   authToken,
	}

	log.Println("config: CacheDir =", cfg.CacheDir)
	log.Println("config: CacheTTL =", cfg.CacheTTL)
	log.Println("config: Port =", cfg.Port)
	log.Println("config: SSLPort =", cfg.SSLPort)
	log.Println("config: AccessToken =", maskSecret(cfg.AccessToken, minSecretLength, maskLength))
	log.Println("config: EnableSSL =", strconv.FormatBool(cfg.EnableSSL))
	log.Println("config: GroupID =", maskSecret(strconv.Itoa(cfg.GroupID), minSecretLength, maskLength))
	log.Println("config: AuthToken =", maskSecret(cfg.AuthToken, minSecretLength, maskLength))

	if authToken == "" {
		log.Println("WARNING: authentication disabled")
	}

	// Validate the key and certificate if provided
	if cfg.EnableSSL {
		log.Println("SSL enabled")

		if err := validateCertAndKey(*certFile, *keyFile); err != nil {
			return nil, fmt.Errorf("failed to load TLS certificate: %w", err)
		}

		log.Println("SSL certificate loaded successfully")

		// SSL credentials are valid
		cfg.CertFile = *certFile
		cfg.KeyFile = *keyFile
	} else {
		// No SSL
		log.Println("WARNING: SSL disabled")
	}

	// Return the parsed and validated configuration
	return cfg, nil
}

// isValidPort checks if the port number is within the valid range of 1 to 65535.
func isValidPort(port int) bool {
	return port > 0 && port <= 65535
}

// isValidTTL checks if the TTL is within the valid range of 60 (1 minute) to 604800 (1 week).
func isValidTTL(ttl int) bool {
	return ttl >= 60 && ttl <= 604800
}

func validateCertAndKey(certFile, keyFile string) error {
	// Discard the results as we only need to validate here
	_, err := loadCert(certFile, keyFile)
	if err != nil {
		return fmt.Errorf("failed to load certificate and key: %w", err)
	}

	return nil
}

func maskSecret(secret string, minLen, numStars int) string {
	stars := strings.Repeat("*", numStars)
	if len(secret) < minLen {
		return stars
	}

	return stars + secret[len(secret)-3:]
}
