package application

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// Printer defines an interface for printing data in various formats.
type Printer interface {
	// Print outputs the data in a human-readable format.
	Print(w http.ResponseWriter, data domain.Collection)
	// JSON outputs the data in JSON format.
	JSON(w http.ResponseWriter, data domain.Collection)
}

var (
	// DefaultPrinter is an instance of the defaultPrinter.
	DefaultPrinter = &defaultPrinter{}
	encoderFactory = defaultEncoderFactory
)

// defaultPrinter provides default implementations for printing data.
type defaultPrinter struct{}

func defaultEncoderFactory(w http.ResponseWriter) *json.Encoder {
	return json.NewEncoder(w)
}

// Print outputs the data in a human-readable format.
func (p *defaultPrinter) Print(writer http.ResponseWriter, data domain.Collection) {
	for _, item := range data {
		_, _ = fmt.Fprintf(writer, "%s (@%s)\n", item.Name, item.Username)
		_, _ = fmt.Fprintf(writer, "Groups:    %s\n", item.Groups)
		_, _ = fmt.Fprintf(writer, "Projects:  %s\n\n", item.Projects)
	}

	_, _ = fmt.Fprintf(writer, "Total Users: %d\n", len(data))
}

// JSON outputs the data in JSON format.
func (p *defaultPrinter) JSON(w http.ResponseWriter, data domain.Collection) {
	// Create a new encoder that writes to w.
	encoder := encoderFactory(w)

	// Encode the responseData struct to JSON and write it to the ResponseWriter.
	w.Header().Set("Content-Type", "application/json")

	if err := encoder.Encode(data); err != nil {
		log.Println("failed to encode response: ", err)
		// Handle error in case the encoding fails.
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)

		return
	}
}
