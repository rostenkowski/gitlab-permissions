package application

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// errorResponseWriter simulates an error when writing the response.
type errorResponseWriter struct {
	http.ResponseWriter
}

func (erw *errorResponseWriter) Write(_ []byte) (int, error) {
	return 0, errors.New("simulated write error")
}

// errorEncoderFactory returns an encoder that uses errorResponseWriter.
func errorEncoderFactory(w http.ResponseWriter) *json.Encoder {
	return json.NewEncoder(&errorResponseWriter{w})
}

func TestPrintJSON(t *testing.T) {
	tests := []struct {
		name           string
		data           domain.Collection
		encoderFactory func(w http.ResponseWriter) *json.Encoder
		want           string
		wantStatus     int
		wantError      bool
	}{
		{
			name:           "Empty collection",
			data:           domain.Collection{},
			encoderFactory: defaultEncoderFactory,
			want:           "[]\n",
			wantStatus:     http.StatusOK,
			wantError:      false,
		},
		{
			name: "Non-empty collection",
			data: domain.Collection{
				&domain.User{
					Name:     "John Doe",
					Username: "johndoe",
					Groups:   domain.GroupAccessList{{Group: domain.Group{Path: "Dev"}, Role: domain.Access(gitlab.DeveloperPermissions)}},
					Projects: domain.ProjectAccessList{{Project: &domain.Project{Path: "Project XXX"}, Role: domain.Access(gitlab.ReporterPermissions)}},
				},
			},
			encoderFactory: defaultEncoderFactory,
			want:           `[{"Name":"John Doe","Username":"johndoe","Groups":[{"Group":{"Path":"Dev"},"Role":"Developer"}],"Projects":[{"Project":{"Path":"Project XXX"},"Role":"Reporter"}]}]` + "\n",
			wantStatus:     http.StatusOK,
			wantError:      false,
		},
		{
			name:           "Error in JSON encoding",
			data:           domain.Collection{},
			encoderFactory: errorEncoderFactory,
			want:           "Error encoding JSON\n",
			wantStatus:     http.StatusInternalServerError,
			wantError:      true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set up HTTP test recorder
			rr := httptest.NewRecorder()

			// Set the EncoderFactory to the desired factory for this test
			encoderFactory = tt.encoderFactory

			// Execute function
			DefaultPrinter.JSON(rr, tt.data)

			// Check response body and status code
			assert.Equal(t, tt.wantStatus, rr.Code)
			response := rr.Body.String()
			assert.Equal(t, tt.want, response)
		})
	}
}

// Print

// TestDefaultPrinter_Print tests the Print method of the defaultPrinter.
func TestDefaultPrinter_Print(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		data     domain.Collection
		expected string
	}{
		{
			name:     "Empty data",
			data:     domain.Collection{},
			expected: "Total Users: 0\n",
		},
		{
			name: "Single user, no groups or projects",
			data: domain.Collection{
				{
					ID: 1, Name: "John Doe", Username: "johndoe",
				},
			},
			expected: "John Doe (@johndoe)\nGroups:    []\nProjects:  []\n\nTotal Users: 1\n",
		},
		{
			name: "Single user with groups and projects",
			data: domain.Collection{
				{
					ID:   1,
					Name: "John Doe", Username: "johndoe",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{ID: 1, Path: "group1"}, Role: domain.Access(gitlab.OwnerPermissions)},
						{Group: domain.Group{ID: 2, Path: "group2"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{Project: &domain.Project{ID: 1, Path: "project1"}, Role: domain.Access(gitlab.MaintainerPermissions)},
						domain.ProjectAccess{Project: &domain.Project{ID: 2, Path: "project2"}, Role: domain.Access(gitlab.ReporterPermissions)},
					},
				},
			},
			expected: "John Doe (@johndoe)\nGroups:    [group1 (Owner), group2 (Developer)]\nProjects:  [project1 (Maintainer), project2 (Reporter)]\n\nTotal Users: 1\n",
		},
		{
			name: "Multiple users",
			data: domain.Collection{
				{
					Name: "John Doe", Username: "johndoe",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{Path: "group1"}, Role: domain.Access(gitlab.OwnerPermissions)},
					},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{Path: "project1"}, Role: domain.Access(gitlab.MaintainerPermissions)},
					},
				},
				{
					Name: "Jane Smith", Username: "janesmith",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{Path: "group2"}, Role: domain.Access(gitlab.MaintainerPermissions)},
					},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{Path: "project2"}, Role: domain.Access(gitlab.ReporterPermissions)},
					},
				},
			},
			expected: "John Doe (@johndoe)\nGroups:    [group1 (Owner)]\nProjects:  [project1 (Maintainer)]\n\nJane Smith (@janesmith)\nGroups:    [group2 (Maintainer)]\nProjects:  [project2 (Reporter)]\n\nTotal Users: 2\n",
		},
	}

	for _, tt := range tests {
		testCase := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			// Initialize the defaultPrinter
			printer := &defaultPrinter{}

			// Create a ResponseRecorder to capture the output
			httpRecorder := httptest.NewRecorder()

			// Call the Print method
			printer.Print(httpRecorder, testCase.data)

			// Compare the actual output with the expected output
			assert.Equal(t, testCase.expected, httpRecorder.Body.String())
		})
	}
}
