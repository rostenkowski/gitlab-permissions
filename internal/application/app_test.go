package application

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/config"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/store"
)

// PrinterMock is a mock implementation of the Printer interface.
type PrinterMock struct{}

func (m *PrinterMock) Print(w http.ResponseWriter, _ domain.Collection) {
	_, _ = w.Write([]byte("Printed Data"))
}

func (m *PrinterMock) JSON(w http.ResponseWriter, _ domain.Collection) {
	_, _ = w.Write([]byte("Printed JSON Data"))
}

// Run
// ..
//

var (
	app      *App
	httpsApp *App
)

func createTestingCertificate() {
	// Define the OpenSSL command
	cmd := exec.Command("openssl", "req", "-x509", "-nodes", "-days", "365", "-newkey", "rsa:2048", "-keyout", "key.pem", "-out", "cert.pem", "-subj", "/C=US/ST=California/L=San Francisco/O=Example Company/OU=IT Department/CN=example.com/emailAddress=admin@example.com")

	// Run the command
	err := cmd.Run()
	if err != nil {
		log.Fatalf("failed to create testing cert: %s\n", err)
	}
}

func deleteTestingCertificate() {
	if err := os.Remove("key.pem"); err != nil {
		log.Fatal("failed to remove testing key: ", err)
	}

	if err := os.Remove("cert.pem"); err != nil {
		log.Fatal("failed to remove testing cert: ", err)
	}
}

func TestMain(m *testing.M) {
	// Initialize the App
	app = &App{
		Storage: store.NewMemoryStorage(false, domain.Collection{}),
		Server:  &http.Server{Addr: ":8080", ReadHeaderTimeout: 1 * time.Second},
		printer: &PrinterMock{},
		config: &config.Config{
			Port:        8080,
			CacheDir:    "./cache-test1",
			CacheTTL:    701,
			AccessToken: "token1",
			GroupID:     111,
			EnableSSL:   false,
			KeyFile:     "",
			CertFile:    "",
		},
	}

	createTestingCertificate()

	// Initialize the App for the HTTPS server
	httpsApp = &App{
		Storage:   store.NewMemoryStorage(false, domain.Collection{}),
		Server:    &http.Server{Addr: ":9003", ReadHeaderTimeout: 1 * time.Second},
		SSLServer: &http.Server{Addr: ":9004", ReadHeaderTimeout: 1 * time.Second},
		printer:   &PrinterMock{},
		config: &config.Config{
			Port:        9003,
			SSLPort:     9004,
			CacheDir:    "./cache-test443",
			CacheTTL:    7443,
			AccessToken: "token443",
			GroupID:     1443,
			EnableSSL:   true,
			KeyFile:     "key.pem",
			CertFile:    "cert.pem",
		},
	}

	// Create a custom ServeMux for the HTTP server
	mux := http.NewServeMux()

	// Create a custom ServeMux for the HTTPS server
	muxHTTP := http.NewServeMux()
	muxHTTPS := http.NewServeMux()

	// Start the Server in a separate goroutine
	go func() {
		app.Run(mux)
	}()

	// Start the HTTPS Server in a separate goroutine
	go func() {
		httpsApp.Run(muxHTTP, muxHTTPS)
	}()

	// Allow some time for the Server to start
	time.Sleep(1000 * time.Millisecond)

	// Run the tests
	code := m.Run()

	// Shutdown the Server
	if err := app.Server.Shutdown(context.Background()); err != nil {
		panic(err)
	}

	if httpsErr := httpsApp.Server.Shutdown(context.Background()); httpsErr != nil {
		panic(httpsErr)
	}

	deleteTestingCertificate()

	// Exit with the code from the tests
	os.Exit(code)
}

func TestApp_Run_ListenAndServeError(t *testing.T) {
	// Initialize the App
	app := &App{
		Storage: store.NewMemoryStorage(false, domain.Collection{}),
		Server:  &http.Server{Addr: ":8080", ReadHeaderTimeout: 1 * time.Second},
		printer: &PrinterMock{},
		config: &config.Config{
			Port:        9002,
			CacheDir:    "./cache-test9002",
			CacheTTL:    902,
			AccessToken: "token9002",
			GroupID:     111,
			EnableSSL:   false,
			KeyFile:     "",
			CertFile:    "",
		},
	}

	// Create a safe buffer for capturing log output
	var logBuffer SafeBuffer

	log.SetOutput(&logBuffer)

	defer log.SetOutput(io.Discard)

	// Occupy the port by starting a temporary server
	shutdownTempServer := startTempServer("8080")
	defer shutdownTempServer()

	// Create a custom ServeMux
	mux := http.NewServeMux()

	// Run the app, which should fail to start the server
	go app.Run(mux)

	// Allow some time for the app to attempt to start the server
	time.Sleep(100 * time.Millisecond)

	// Check for expected log entry
	expectedLogEntry := "HTTP server error: listen tcp :8080: bind: address already in use\n"
	assert.Contains(t, logBuffer.String(), expectedLogEntry)
}

func TestApp_HTTPS_Run_ListenAndServeError(t *testing.T) {
	// Initialize the App
	app := &App{
		Storage:   store.NewMemoryStorage(false, domain.Collection{}),
		Server:    &http.Server{Addr: ":9010", ReadHeaderTimeout: 1 * time.Second},
		SSLServer: &http.Server{Addr: ":9011", ReadHeaderTimeout: 1 * time.Second},
		printer:   &PrinterMock{},
		config: &config.Config{
			Port:        9010,
			SSLPort:     9011,
			CacheDir:    "./cache-test9003",
			CacheTTL:    903,
			AccessToken: "token9003",
			GroupID:     111,
			EnableSSL:   true,
			KeyFile:     "", // INVALID KEY PATH
			CertFile:    "", // INVALID CERT PATH
		},
	}

	// Create a safe buffer for capturing log output
	var logBuffer SafeBuffer

	log.SetOutput(&logBuffer)

	defer log.SetOutput(io.Discard)

	// Create a custom ServeMux
	mux := http.NewServeMux()

	// Run the app, which should fail to start the server
	go app.Run(mux)

	// Allow some time for the app to attempt to start the server
	time.Sleep(100 * time.Millisecond)

	// This test fail is caused by missing certificate when starting the server

	// Check for expected log entry

	// There should be check before trying to run server with invalid certificate.
	// Such a check is in the config, and normally it would check before this error can occur.
	// Here we are not running these check as the config is prepared on-the-fly.
	expectedLogEntry := "HTTPS server error: open : no such file or directory"
	assert.Contains(t, logBuffer.String(), expectedLogEntry)
}

func TestApp_Run(t *testing.T) {
	// Define the test cases
	tests := []struct {
		name           string
		initialized    bool
		expectedCode   int
		expectedBody   string
		url            string
		useMockPrinter bool
	}{
		{
			name:         "Root endpoint when ready",
			initialized:  true,
			expectedCode: http.StatusOK,
			expectedBody: "Printed Data",
			url:          "/",
		},
		{
			name:         "Root endpoint when not ready",
			initialized:  false,
			expectedCode: http.StatusServiceUnavailable,
			expectedBody: http.StatusText(http.StatusServiceUnavailable),
			url:          "/",
		},
		{
			name:         "JSON endpoint when ready",
			initialized:  true,
			expectedCode: http.StatusOK,
			expectedBody: "Printed JSON Data",
			url:          "/json",
		},
		{
			name:         "JSON endpoint when not ready",
			initialized:  false,
			expectedCode: http.StatusServiceUnavailable,
			expectedBody: http.StatusText(http.StatusServiceUnavailable),
			url:          "/json",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set the initialization state
			app.Storage = store.NewMemoryStorage(tt.initialized, domain.Collection{})

			// Create context
			ctx := context.Background()

			// Create a new request with the context
			req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://"+app.Server.Addr+tt.url, nil)
			if err != nil {
				t.Fatalf("failed to create request: %v", err)
			}

			client := &http.Client{}

			// Perform the request
			resp, err := client.Do(req)
			if err != nil {
				t.Fatalf("failed to send request: %v", err)
			}

			defer func() {
				_ = resp.Body.Close()
			}()

			// Read the response body
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("failed to read response body: %v", err)
			}

			// Check the status code
			if resp.StatusCode != tt.expectedCode {
				t.Errorf("expected status code %v, got %v", tt.expectedCode, resp.StatusCode)
			}

			// Check the response body
			if !strings.Contains(string(body), tt.expectedBody) {
				t.Errorf("expected body %v, got %v", tt.expectedBody, string(body))
			}

			// ---

			// Set the initialization state
			httpsApp.Storage = store.NewMemoryStorage(tt.initialized, domain.Collection{})

			// Create HTTPS client with custom transport to skip certificate verification
			httpsClient := &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true, // #nosec G402
					},
				},
			}

			// Create context
			httpsCtx := context.Background()

			// Create a new request with the context
			// Note: We request http:// and follow the redirect to https://
			httpsReq, httpsErr := http.NewRequestWithContext(httpsCtx, http.MethodGet, "http://localhost"+httpsApp.Server.Addr+tt.url, nil)
			if httpsErr != nil {
				t.Fatalf("failed to create request: %v", err)
			}

			// Perform the request
			httpsResp, httpsErr := httpsClient.Do(httpsReq)
			if httpsErr != nil {
				t.Fatalf("failed to send HTTPS request: %v", httpsErr)
			}

			defer func() {
				_ = httpsResp.Body.Close()
			}()

			// Read the response body
			httpsBody, httpsErr := io.ReadAll(httpsResp.Body)
			if httpsErr != nil {
				t.Fatalf("HTTPS: %s: failed to read response body: %v", tt.name, httpsErr)
			}

			// Check the status code
			if httpsResp.StatusCode != tt.expectedCode {
				t.Errorf("HTTPS: %s: expected status code %v, got %v", tt.name, tt.expectedCode, httpsResp.StatusCode)
			}

			// Check the response body
			if !strings.Contains(string(httpsBody), tt.expectedBody) {
				t.Errorf("HTTPS: %s: expected body %v, got %v", tt.name, tt.expectedBody, string(httpsBody))
			}
		})
	}
}

func TestApp_Authorization(t *testing.T) {
	// Define the test cases
	tests := []struct {
		name           string
		setAuth        string
		sendHeader     string
		expectedCode   int
		expectedBody   string
		url            string
		useMockPrinter bool
	}{
		{
			name:         "Auth success (/ endpoint)",
			setAuth:      "foo",
			sendHeader:   "Bearer foo",
			expectedCode: http.StatusOK,
			expectedBody: "Printed Data",
			url:          "/",
		},
		{
			name:         "Auth success (/json endpoint)",
			setAuth:      "foo",
			sendHeader:   "Bearer foo",
			expectedCode: http.StatusOK,
			expectedBody: "Printed Data",
			url:          "/",
		},
		{
			name:         "Auth failure (/ endpoint)",
			setAuth:      "foo",
			sendHeader:   "Bearer bar",
			expectedCode: http.StatusUnauthorized,
			expectedBody: "invalid credentials",
			url:          "/",
		},
		{
			name:         "Auth failure (/json endpoint)",
			setAuth:      "foo",
			sendHeader:   "Bearer bar",
			expectedCode: http.StatusUnauthorized,
			expectedBody: "invalid credentials",
			url:          "/json",
		},
		{
			name:         "No auth",
			setAuth:      "",
			sendHeader:   "",
			expectedCode: http.StatusOK,
			expectedBody: "Printed Data",
			url:          "/",
		},
		{
			name:         "Auth provided when disabled (no problem)",
			setAuth:      "",
			sendHeader:   "Bearer invalid",
			expectedCode: http.StatusOK,
			expectedBody: "Printed Data",
			url:          "/",
		},
		{
			name:         "Invalid credentials",
			setAuth:      "secret",
			sendHeader:   "wrong format",
			expectedCode: http.StatusUnauthorized,
			expectedBody: "invalid credentials",
			url:          "/",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Simulate initialized application storage
			httpsApp.Storage = store.NewMemoryStorage(true, domain.Collection{})

			// 1. Set the required AuthToken sendHeader
			httpsApp.config.AuthToken = tt.setAuth

			// Create HTTPS client with custom transport to skip certificate verification
			httpsClient := &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true, // #nosec G402
					},
				},
			}

			// Create context
			httpsCtx := context.Background()

			// Create a new request with the context
			httpsReq, httpsErr := http.NewRequestWithContext(httpsCtx, http.MethodGet, "http://localhost"+httpsApp.Server.Addr+tt.url, nil)
			if httpsErr != nil {
				t.Fatalf("failed to create request: %v", httpsErr)
			}

			// Add a header to the request
			httpsReq.Header.Set("Authorization", tt.sendHeader)

			// Perform the request
			httpsResp, httpsErr := httpsClient.Do(httpsReq)
			if httpsErr != nil {
				t.Fatalf("failed to send HTTPS request: %v", httpsErr)
			}

			defer func() {
				_ = httpsResp.Body.Close()
			}()

			// Read the response body
			httpsBody, httpsErr := io.ReadAll(httpsResp.Body)
			if httpsErr != nil {
				t.Fatalf("HTTPS: %s: failed to read response body: %v", tt.name, httpsErr)
			}

			// Check the status code
			if httpsResp.StatusCode != tt.expectedCode {
				t.Errorf("HTTPS: %s: expected status code %v, got %v", tt.name, tt.expectedCode, httpsResp.StatusCode)
			}

			// Check the response body
			if !strings.Contains(string(httpsBody), tt.expectedBody) {
				t.Errorf("HTTPS: %s: expected body %v, got %v", tt.name, tt.expectedBody, string(httpsBody))
			}
		})
	}
}

func TestApp_Run_DefaultServeMux(t *testing.T) {
	// Create a safe buffer for capturing log output
	var logBuffer SafeBuffer

	log.SetOutput(&logBuffer)

	defer log.SetOutput(io.Discard)

	// Create a new app instance with default dependencies
	app := &App{
		Storage: store.NewMemoryStorage(true, domain.Collection{{ID: 1, Name: "MockUser", Username: "mockuser"}}),
		Server:  &http.Server{Addr: ":8081", ReadHeaderTimeout: 1 * time.Second}, // Use a different port to avoid conflicts
		printer: &PrinterMock{},
		config: &config.Config{
			Port:        8080,
			CacheDir:    "./cache-test2",
			CacheTTL:    702,
			AccessToken: "token2",
			GroupID:     222,
			EnableSSL:   false,
			KeyFile:     "",
			CertFile:    "",
		},
	}

	// Run the app without providing a custom ServeMux
	go app.Run()

	// Allow some time for the app to start
	time.Sleep(100 * time.Millisecond)

	// Create context
	ctx := context.Background()

	// Create a new request with the context
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://"+app.Server.Addr+"/", nil)
	if err != nil {
		t.Fatalf("failed to create request: %v", err)
	}

	client := &http.Client{}

	// Perform the request
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("failed to send request: %v", err)
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("failed to read response body: %v", err)
	}

	// Check the status code and body
	expectedCode := http.StatusOK
	expectedBody := "Printed Data"

	if resp.StatusCode != expectedCode {
		t.Errorf("expected status code %v, got %v", expectedCode, resp.StatusCode)
	}

	if !strings.Contains(string(body), expectedBody) {
		t.Errorf("expected body %v, got %v", expectedBody, string(body))
	}

	// Shutdown the server
	if err := app.Server.Shutdown(context.Background()); err != nil {
		t.Fatalf("server failed to shut down: %s", err)
	}
}

func TestApp_Run_CustomServeMux(t *testing.T) {
	// Create a safe buffer for capturing log output
	var logBuffer SafeBuffer

	log.SetOutput(&logBuffer)

	defer log.SetOutput(io.Discard)

	// Create a new app instance with default dependencies
	app := &App{
		Storage: store.NewMemoryStorage(true, domain.Collection{{ID: 1, Name: "MockUser", Username: "mockuser"}}),
		Server:  &http.Server{Addr: ":8081", ReadHeaderTimeout: 1 * time.Second}, // Use a different port to avoid conflicts
		printer: &PrinterMock{},
		config: &config.Config{
			Port:        8080,
			CacheDir:    "./cache-test2",
			CacheTTL:    702,
			AccessToken: "token2",
			GroupID:     222,
			EnableSSL:   false,
			KeyFile:     "",
			CertFile:    "",
		},
	}

	// Run the app providing a custom multiplexers for both HTTP and HTTPS
	go app.Run(http.NewServeMux(), http.NewServeMux())

	// Allow some time for the app to start
	time.Sleep(100 * time.Millisecond)

	// Create context
	ctx := context.Background()

	// Create a new request with the context
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://"+app.Server.Addr+"/", nil)
	if err != nil {
		t.Fatalf("failed to create request: %v", err)
	}

	client := &http.Client{}

	// Perform the request
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("failed to send request: %v", err)
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("failed to read response body: %v", err)
	}

	// Check the status code and body
	expectedCode := http.StatusOK
	expectedBody := "Printed Data"

	if resp.StatusCode != expectedCode {
		t.Errorf("expected status code %v, got %v", expectedCode, resp.StatusCode)
	}

	if !strings.Contains(string(body), expectedBody) {
		t.Errorf("expected body %v, got %v", expectedBody, string(body))
	}

	// Shutdown the server
	if err := app.Server.Shutdown(context.Background()); err != nil {
		t.Fatalf("server failed to shut down: %s", err)
	}
}

// SafeBuffer is a thread-safe buffer for capturing log output.
type SafeBuffer struct {
	mu     sync.Mutex
	buffer bytes.Buffer
}

func (b *SafeBuffer) Write(p []byte) (int, error) {
	b.mu.Lock()

	defer b.mu.Unlock()

	n, err := b.buffer.Write(p)

	if err != nil {
		return n, fmt.Errorf("failed to write buffer: %w", err)
	}

	return n, nil
}

func (b *SafeBuffer) String() string {
	b.mu.Lock()

	defer b.mu.Unlock()

	return b.buffer.String()
}

// startTempServer starts a temporary server to occupy the port and returns a function to shut it down.
func startTempServer(port string) func() {
	server := &http.Server{Addr: ":" + port, ReadHeaderTimeout: 10 * time.Second}
	go func() {
		_ = server.ListenAndServe()
	}()
	time.Sleep(100 * time.Millisecond) // Allow some time for the server to start

	return func() {
		_ = server.Shutdown(context.Background())
	}
}

// -----
// WATCH
// -----

type WatchCollectorMock struct {
	mock.Mock
	mockData domain.Collection
	mockErr  error
}

func (m *WatchCollectorMock) Collect(ctx context.Context) (domain.Collection, error) {
	args := m.Called(ctx)
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}

	// Attempt to retrieve the collection from the function call's return values
	collection, ok := args.Get(0).(domain.Collection)
	if !ok {
		// If the type assertion fails, return an error indicating the unexpected type
		return nil, errors.New("unexpected type returned from function call")
	}

	// Return the retrieved collection and nil error
	return collection, nil
}

type WatchStorageMock struct {
	mock.Mock
	lock        sync.RWMutex
	currentData domain.Collection
	initialized bool
}

func (m *WatchStorageMock) GetData() domain.Collection {
	m.lock.RLock()

	defer m.lock.RUnlock()

	return m.currentData
}

func (m *WatchStorageMock) SetData(data domain.Collection) {
	m.lock.Lock()

	defer m.lock.Unlock()

	m.currentData = data
	m.initialized = true
	m.Called(data)
}

func (m *WatchStorageMock) Ready() bool {
	m.lock.RLock()

	defer m.lock.RUnlock()

	return m.initialized
}

func TestApp_Watch2(t *testing.T) {
	// Redirect log output to avoid cluttering test output
	log.SetOutput(io.Discard)

	// Create mock instances
	mockCollector := &WatchCollectorMock{
		mockData: domain.Collection{{ID: 1, Name: "MockUser", Username: "mockuser"}},
		mockErr:  nil,
	}
	mockCollector.On("Collect", mock.Anything).Return(mockCollector.mockData, nil)

	mockStorage := &WatchStorageMock{}
	mockStorage.On("SetData", mockCollector.mockData).Return().Times(3)

	// Create a new app instance with mock dependencies
	app := NewApp(&config.Config{
		Port:        9000,
		CacheDir:    "./cache-test3",
		CacheTTL:    703,
		AccessToken: "token3",
		GroupID:     333,
	}, mockCollector, mockStorage, 50*time.Millisecond) // Use a shorter interval for testing

	// Create a context with a timeout to simulate cancellation
	ctx, cancel := context.WithTimeout(context.Background(), 160*time.Millisecond)
	defer cancel()

	// Start the Watch method in a separate goroutine
	testCacheDir := "./cache-test"
	go app.Watch(ctx, &testCacheDir)

	// Allow some time for the Watch method to run
	time.Sleep(160 * time.Millisecond)

	// Verify that Collect and SetData were called the expected number of times
	mockCollector.AssertNumberOfCalls(t, "Collect", 3)
	mockStorage.AssertNumberOfCalls(t, "SetData", 3)
}

// WATCH2 ERROR

type WatchCollectorErrorCollectorMock struct {
	mock.Mock
	mockData domain.Users
	mockErr  error
}

func (m *WatchCollectorErrorCollectorMock) Collect(ctx context.Context) (domain.Collection, error) {
	args := m.Called(ctx)
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(domain.Collection), nil
}

type WatchCollectorErrorStorageMock struct {
	mock.Mock
	lock        sync.RWMutex
	currentData domain.Collection
	initialized bool
}

func (m *WatchCollectorErrorStorageMock) GetData() domain.Collection {
	m.lock.RLock()

	defer m.lock.RUnlock()

	return m.currentData
}

func (m *WatchCollectorErrorStorageMock) SetData(data domain.Collection) {
	m.lock.Lock()

	defer m.lock.Unlock()

	m.currentData = data
	m.initialized = true
	m.Called(data)
}

func (m *WatchCollectorErrorStorageMock) Ready() bool {
	m.lock.RLock()

	defer m.lock.RUnlock()

	return m.initialized
}

func TestApp_Watch_CollectError(t *testing.T) {
	var logBuffer SafeBuffer

	log.SetOutput(&logBuffer)

	defer log.SetOutput(io.Discard)

	// Create mock instances
	mockCollector := &WatchCollectorErrorCollectorMock{
		mockData: domain.Users{},
		mockErr:  errors.New("mock collection error"),
	}
	mockCollector.On("Collect", mock.Anything).Return(mockCollector.mockData, mockCollector.mockErr)

	mockStorage := &WatchCollectorErrorStorageMock{}
	mockStorage.On("SetData", mockCollector.mockData).Return()

	// Create a new app instance with mock dependencies
	app := NewApp(&config.Config{
		Port:        9000,
		CacheDir:    "./cache-test4",
		CacheTTL:    704,
		AccessToken: "token4",
		GroupID:     444,
	}, mockCollector, mockStorage, 50*time.Millisecond) // Use a shorter interval for testing

	// Create a context with a timeout to simulate cancellation
	ctx, cancel := context.WithTimeout(context.Background(), 160*time.Millisecond)
	defer cancel()

	// Start the Watch method in a separate goroutine
	testCacheDir := "./cache-test2"
	go app.Watch(ctx, &testCacheDir)

	// Allow some time for the Watch method to run
	time.Sleep(160 * time.Millisecond)

	// Verify that Collect was called the expected number of times
	mockCollector.AssertNumberOfCalls(t, "Collect", 3)

	// Verify that SetData was not called
	mockStorage.AssertNumberOfCalls(t, "SetData", 0)

	// Check for expected log entry
	expectedLogEntry := "failed to collect data from gitlab: mock collection error\n"
	assert.Contains(t, logBuffer.String(), expectedLogEntry)
}

// ----
// INIT
// ----

type MockStorage3 struct {
	lock        sync.RWMutex
	currentData domain.Collection
	initialized bool
}

func (m *MockStorage3) GetData() domain.Collection {
	m.lock.RLock()

	defer m.lock.RUnlock()

	return m.currentData
}

func (m *MockStorage3) SetData(data domain.Collection) {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.currentData = data
	m.initialized = true
}

func (m *MockStorage3) Ready() bool {
	m.lock.RLock()
	defer m.lock.RUnlock()

	return m.initialized
}

// Init
// ..
//

func TestApp_Init(t *testing.T) {
	// Redirect log output to avoid cluttering test output
	log.SetOutput(io.Discard)

	// Define the test cases
	tests := []struct {
		name         string
		mockData     domain.Collection
		mockErr      error
		expectedData domain.Collection
		expectedInit bool
	}{
		{
			name:         "Successful data collection",
			mockData:     domain.Collection{{ID: 1, Name: "MockUser", Username: "mockuser"}},
			mockErr:      nil,
			expectedData: domain.Collection{{ID: 1, Name: "MockUser", Username: "mockuser"}},
			expectedInit: true,
		},
		{
			name:         "Data collection failure",
			mockData:     nil,
			mockErr:      errors.New("mock error"),
			expectedData: nil,
			expectedInit: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create mock instances
			mockCollector := &MockCollector{
				mockData: tt.mockData,
				mockErr:  tt.mockErr,
			}

			if tt.mockErr != nil {
				mockCollector.On("Collect", mock.Anything).Return(nil, tt.mockErr)
			} else {
				mockCollector.On("Collect", mock.Anything).Return(mockCollector.mockData, nil)
			}

			mockStorage := &MockStorage3{}

			// Create a new app instance with mock dependencies
			app := NewApp(&config.Config{
				Port:        9000,
				CacheDir:    "./cache-test5",
				CacheTTL:    705,
				AccessToken: "token5",
				GroupID:     555,
			}, mockCollector, mockStorage, 60*time.Second)

			// Call the Init method
			app.Init(context.Background())

			// Check if SetData was called with the correct data
			mockStorage.lock.RLock()

			defer mockStorage.lock.RUnlock()

			if mockStorage.initialized != tt.expectedInit {
				t.Errorf("expected initialized to be %v, got %v", tt.expectedInit, mockStorage.initialized)
			}

			if !equalCollections(mockStorage.currentData, tt.expectedData) {
				t.Errorf("expected currentData to be %v, got %v", tt.expectedData, mockStorage.currentData)
			}
		})
	}
}

func equalCollections(a, b domain.Collection) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i].Name != b[i].Name || a[i].Username != b[i].Username {
			return false
		}
	}

	return true
}

// Shutdown
// ...
//

type MockCollector struct {
	mock.Mock
	mockData domain.Collection
	mockErr  error
}

func (m *MockCollector) Collect(ctx context.Context) (domain.Collection, error) {
	args := m.Called(ctx)
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(domain.Collection), nil
}

func TestExtractBearerToken(t *testing.T) {
	tests := []struct {
		name          string
		authHeader    string
		expectedToken string
		expectError   bool
	}{
		{
			name:          "Valid token",
			authHeader:    "Bearer valid_token",
			expectedToken: "valid_token",
			expectError:   false,
		},
		{
			name:          "Missing AuthToken header",
			authHeader:    "",
			expectedToken: "",
			expectError:   true,
		},
		{
			name:          "Invalid format (no Bearer prefix)",
			authHeader:    "invalid_token",
			expectedToken: "",
			expectError:   true,
		},
		{
			name:          "Invalid format (Bearer prefix but no token)",
			authHeader:    "Bearer ",
			expectedToken: "",
			expectError:   true,
		},
		{
			name:          "Invalid format (extra spaces)",
			authHeader:    "Bearer  invalid_token",
			expectedToken: " invalid_token",
			expectError:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/", nil)
			if tt.authHeader != "" {
				req.Header.Set("Authorization", tt.authHeader)
			}

			token, err := extractBearerToken(tt.authHeader)
			if (err != nil) != tt.expectError {
				t.Errorf("Expected error: %v, got: %v", tt.expectError, err)
			}

			if token != tt.expectedToken {
				t.Errorf("Expected token: %s, got: %s", tt.expectedToken, token)
			}
		})
	}
}
