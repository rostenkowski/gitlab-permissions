package application

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/collect"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/config"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/filesystem"
	"gitlab.com/rostenkowski/gitlab-permissions/internal/store"
)

const (
	readTimeout  = 10 * time.Second
	writeTimeout = 10 * time.Second
	idleTimeout  = 10 * time.Second
)

// App encapsulates all the application dependencies.
type App struct {
	Server       *http.Server
	SSLServer    *http.Server
	printer      Printer
	config       *config.Config
	collector    collect.Collector
	Storage      store.Storage
	tickInterval time.Duration
}

// NewApp creates a new instance of App with the provided configuration, collector, and storage.
func NewApp(cfg *config.Config, collector collect.Collector, storage store.Storage, tickInterval time.Duration) *App {
	return &App{
		Storage: storage,
		Server: &http.Server{
			Addr:         fmt.Sprintf(":%d", cfg.Port),
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
			IdleTimeout:  idleTimeout,
		},
		SSLServer: &http.Server{
			Addr:         fmt.Sprintf(":%d", cfg.SSLPort),
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
			IdleTimeout:  idleTimeout,
		},
		collector:    collector,
		config:       cfg,
		printer:      DefaultPrinter,
		tickInterval: tickInterval,
	}
}

// Run sets up the handlers and starts the HTTP server.
func (a *App) Run(mux ...*http.ServeMux) {
	var handler *http.ServeMux
	if len(mux) > 0 {
		handler = mux[0]
	} else {
		handler = http.NewServeMux()
	}

	handler.HandleFunc("/", a.handleText)
	handler.HandleFunc("/json", a.handleJSON)

	a.Server.Handler = handler

	if a.config.EnableSSL {
		var httpsHandler *http.ServeMux
		if len(mux) > 1 {
			httpsHandler = mux[1]
		} else {
			httpsHandler = http.NewServeMux()
		}

		httpsHandler.HandleFunc("/", a.handleText)
		httpsHandler.HandleFunc("/json", a.handleJSON)

		a.SSLServer.Handler = httpsHandler

		a.SSLServer.TLSConfig = &tls.Config{
			MinVersion: tls.VersionTLS12,
			MaxVersion: tls.VersionTLS13,
			CipherSuites: []uint16{
				tls.TLS_AES_128_GCM_SHA256,
				tls.TLS_AES_256_GCM_SHA384,
				tls.TLS_CHACHA20_POLY1305_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
			},
		}

		log.Printf("starting HTTPS server on '%s'\n", a.SSLServer.Addr)

		go func() {
			if err := a.SSLServer.ListenAndServeTLS(a.config.CertFile, a.config.KeyFile); err != nil {
				log.Printf("HTTPS server error: %s\n", err)
			}
		}()
	}

	log.Printf("starting HTTP server on '%s'\n", a.Server.Addr)

	if err := a.Server.ListenAndServe(); err != nil {
		log.Printf("HTTP server error: %s\n", err)
	}
}

// Watch periodically collects and updates the application data.
func (a *App) Watch(ctx context.Context, cacheDir *string) {
	ticker := time.NewTicker(a.tickInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			// Purge cache before each refresh to get the snapshot of the data as consistent as possible
			if err := filesystem.Purge(*cacheDir); err != nil {
				log.Printf("failed to purge cache: %s\n", err)
			}

			// Collect data
			data, err := a.collector.Collect(ctx)
			if err != nil {
				log.Printf("failed to collect data from gitlab: %s\n", err)

				continue
			}

			// Store data
			a.Storage.SetData(data)

		case <-ctx.Done():
			// Exit on cancel
			log.Println("shutting down watcher due to context cancellation")

			return
		}
	}
}

// Init initializes the application by collecting initial data.
func (a *App) Init(ctx context.Context) {
	data, err := a.collector.Collect(ctx)
	if err != nil {
		log.Printf("failed to load data from gitlab: %s\n", err)

		return
	}

	a.Storage.SetData(data)
}

// handleText handles the HTTP request to the "/" endpoint.
func (a *App) handleText(w http.ResponseWriter, r *http.Request) {
	if !a.authenticate(w, r) {
		http.Error(w, "invalid credentials", http.StatusUnauthorized)

		return
	}

	if !a.redirect(w, r) {
		if a.Storage.Ready() {
			a.printer.Print(w, a.Storage.GetData())
		} else {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
		}
	}
}

// handleJSON handles the HTTP request to the "/json" endpoint.
func (a *App) handleJSON(w http.ResponseWriter, r *http.Request) {
	if !a.authenticate(w, r) {
		http.Error(w, "invalid credentials", http.StatusUnauthorized)

		return
	}

	if !a.redirect(w, r) {
		if a.Storage.Ready() {
			a.printer.JSON(w, a.Storage.GetData())
		} else {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
		}
	}
}

// authenticate returns true when the provided AuthToken header matches the configured AuthToken key.
func (a *App) authenticate(_ http.ResponseWriter, r *http.Request) bool {
	// Auth is enabled
	if a.config.AuthToken != "" {
		// Read the raw header value
		header := r.Header.Get("Authorization")

		// Extract the bearer token value
		token, err := extractBearerToken(header)
		if err != nil {
			log.Println("failed to extract bearer token:", err)

			// Error while extracting the token value
			return false
		}

		// Tokens match
		return token == a.config.AuthToken
	}

	// Auth is disabled
	return true
}

const authHeaderParts = 2

// extractBearerToken function extracts the Bearer token value from the request AuthToken header.
func extractBearerToken(authHeader string) (string, error) {
	if authHeader == "" {
		return "", errors.New("authorization header is missing")
	}

	parts := strings.SplitN(authHeader, " ", authHeaderParts)
	if len(parts) != 2 || strings.ToLower(parts[0]) != "bearer" || parts[1] == "" {
		return "", fmt.Errorf("invalid authorization header format: %s", authHeader)
	}

	return parts[1], nil
}

// redirect redirects the HTTP request from http:// to https:// when the SSL is enabled.
func (a *App) redirect(w http.ResponseWriter, r *http.Request) bool {
	if a.config.EnableSSL && r.TLS == nil {
		parts := strings.Split(r.Host, ":")
		redirectURL := fmt.Sprintf("https://%s%s", net.JoinHostPort(parts[0], strconv.Itoa(a.config.SSLPort)), r.RequestURI)
		log.Println("redirect to ", redirectURL)
		http.Redirect(w, r, redirectURL, http.StatusMovedPermanently)

		return true // redirected
	}

	return false // not redirected
}
