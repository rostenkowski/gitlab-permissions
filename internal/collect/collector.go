package collect

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sort"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// Collector defines an interface for collecting data.
type Collector interface {
	// Collect retrieves a collection of data.
	Collect(ctx context.Context) (domain.Collection, error)
}

// NewGitlabCollector creates a new instance of GitlabCollector.
func NewGitlabCollector(id *int, g domain.GroupsService, m domain.ProjectMembersService) (*GitlabCollector, error) {
	return &GitlabCollector{GroupID: id, GroupsService: g, ProjectsMembersService: m}, nil
}

// GitlabCollector collects data from GitLab.
type GitlabCollector struct {
	GroupID                *int
	GroupsService          domain.GroupsService
	UsersService           domain.UsersService
	ProjectsService        domain.ProjectsService
	ProjectsMembersService domain.ProjectMembersService
}

// Collect retrieves data from GitLab, including groups, members, and projects.
func (c *GitlabCollector) Collect(ctx context.Context) (domain.Collection, error) {
	start := time.Now()

	// --- GROUPS ---

	// Fetch root group
	root, _, err := c.GroupsService.GetGroup(*c.GroupID, &gitlab.GetGroupOptions{}, gitlab.WithContext(ctx))
	if err != nil {
		return nil, errors.New("failed to fetch root group")
	}

	// Fetch all subgroups
	allGroups, err := fetchGroups(ctx, c.GroupsService, root.ID)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch groups: %w", err)
	}

	// Add the root group to the map
	rootGroup := domain.NewGroup(root.ID, root.Name, root.FullPath)
	allGroups[root.ID] = rootGroup

	// --- MEMBERSHIPS ---

	// Fetch all group members from all groups
	all, err := fetchGroupMemberships(ctx, c.GroupsService, allGroups)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch members: %w", err)
	}

	// Add direct project-only members
	if _, err := fetchProjectMemberships(ctx, all, c.GroupsService, c.ProjectsMembersService, allGroups); err != nil {
		return nil, fmt.Errorf("failed to fetch project members: %w", err)
	}

	// Filter data
	filtered := domain.Collection{}

	for _, item := range all {
		// Create a fresh copy of projects
		projectsFiltered := domain.ProjectAccessList{}

		// Exclude projects that are no longer in the root group
		for _, project := range item.Projects {
			if strings.HasPrefix(project.Project.Path, rootGroup.Path) {
				projectsFiltered = append(projectsFiltered, project)
			} else {
				log.Println("project ignored:", project.Project.Path)
			}
		}

		// Set the filtered projects back
		item.Projects = projectsFiltered

		// Check if there is something remained, if not exclude whole user
		if len(item.Groups) > 0 || len(item.Projects) > 0 {
			filtered = append(filtered, item)
		} else {
			log.Println("user ignored:", item.Username)
		}
	}

	// Sorting the collection by User.Username as it's unique in contrast with the user.Name
	sort.Slice(filtered, func(i, j int) bool {
		return strings.ToLower(filtered[i].Username) < strings.ToLower(filtered[j].Username)
	})

	log.Printf("data collected in: %d ms\n", time.Since(start).Milliseconds())

	return filtered, nil
}
