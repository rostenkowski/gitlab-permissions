package collect

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

type GroupsMockGroupsService struct {
	mock.Mock
}

func (m *GroupsMockGroupsService) GetGroup(_ interface{}, _ *gitlab.GetGroupOptions, _ ...gitlab.RequestOptionFunc) (*gitlab.Group, *gitlab.Response, error) {
	panic("not implemented on purpose")
}

func (m *GroupsMockGroupsService) ListGroupMembers(_ interface{}, _ *gitlab.ListGroupMembersOptions, _ ...gitlab.RequestOptionFunc) ([]*gitlab.GroupMember, *gitlab.Response, error) {
	panic("not implemented on purpose")
}

func (m *GroupsMockGroupsService) ListGroupProjects(_ interface{}, _ *gitlab.ListGroupProjectsOptions, _ ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	panic("not implemented on purpose")
}

func (m *GroupsMockGroupsService) ListSubGroups(gid interface{}, opt *gitlab.ListSubGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	_ = options
	args := m.Called(gid, opt)

	return args.Get(0).([]*gitlab.Group), args.Get(1).(*gitlab.Response), args.Error(2)
}

func TestFetchAllSubgroups(t *testing.T) {
	tests := []struct {
		name           string
		groupID        int
		setupMock      func(service *GroupsMockGroupsService)
		expectedGroups domain.Groups
		expectError    bool
	}{
		{
			name:    "successfully fetch subgroups",
			groupID: 1,
			setupMock: func(m *GroupsMockGroupsService) {
				m.On("ListSubGroups", "1", mock.Anything).Return([]*gitlab.Group{{ID: 2}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "2", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedGroups: domain.Groups{2: {ID: 2}},
			expectError:    false,
		},
		{
			name:    "fetch two pages of subgroups",
			groupID: 1,
			setupMock: func(m *GroupsMockGroupsService) {
				// First page response for group ID 1
				m.On("ListSubGroups", "1", mock.MatchedBy(func(opt *gitlab.ListSubGroupsOptions) bool {
					return opt.Page == 0 || opt.Page == 1 // Matches the initial call or first page
				})).Return([]*gitlab.Group{
					{ID: 2},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 2, NextPage: 2}, nil)

				// Second page response for group ID 1
				m.On("ListSubGroups", "1", mock.MatchedBy(func(opt *gitlab.ListSubGroupsOptions) bool {
					return opt.Page == 2 // Matches the second page
				})).Return([]*gitlab.Group{
					{ID: 3},
				}, &gitlab.Response{CurrentPage: 2, TotalPages: 2}, nil)

				// Subgroups of ID 2 and ID 3 (no further subgroups)
				m.On("ListSubGroups", "2", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "3", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedGroups: domain.Groups{2: {ID: 2}, 3: {ID: 3}},
			expectError:    false,
		},
		{
			name:    "API error during fetching",
			groupID: 1,
			setupMock: func(m *GroupsMockGroupsService) {
				m.On("ListSubGroups", "1", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{}, errors.New("API error"))
			},
			expectedGroups: nil,
			expectError:    true,
		},
		{
			name:    "successfully fetch recursive subgroups 1",
			groupID: 1,
			setupMock: func(m *GroupsMockGroupsService) {
				m.On("ListSubGroups", "1", mock.Anything).Return([]*gitlab.Group{{ID: 2}, {ID: 3}, {ID: 4}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "2", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "3", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "4", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedGroups: domain.Groups{2: {ID: 2}, 3: {ID: 3}, 4: {ID: 4}},
			expectError:    false,
		},
		{
			name:    "successfully fetch recursive subgroups 2",
			groupID: 1,
			setupMock: func(m *GroupsMockGroupsService) {
				m.On("ListSubGroups", "1", mock.Anything).Return([]*gitlab.Group{{ID: 2}, {ID: 3}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "2", mock.Anything).Return([]*gitlab.Group{{ID: 4}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "3", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "4", mock.Anything).Return([]*gitlab.Group{{ID: 5}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				m.On("ListSubGroups", "5", mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedGroups: domain.Groups{2: {ID: 2}, 3: {ID: 3}, 4: {ID: 4}, 5: {ID: 5}},
			expectError:    false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			mockService := new(GroupsMockGroupsService)
			tc.setupMock(mockService)

			result, err := fetchGroups(ctx, mockService, tc.groupID)
			if tc.expectError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tc.expectedGroups, result)
			}
		})
	}
}
