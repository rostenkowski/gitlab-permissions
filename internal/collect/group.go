package collect

import (
	"context"
	"log"
	"strconv"
	"sync"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

const defaultPageSize = 100

// fetchGroups retrieves all subgroups of a given group concurrently.
func fetchGroups(ctx context.Context, groupService domain.GroupsService, gid int) (domain.Groups, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel() // Ensure all paths cancel the context to stop all goroutines.

	var (
		mutex      sync.Mutex     // Mutex to protect shared allGroups slice and visited map
		wg         sync.WaitGroup // WaitGroup to synchronize goroutines
		fetchError error          // Variable to store any error encountered during fetching
		once       sync.Once      // Ensure fetchError is set only once
	)

	allGroups := make(domain.Groups) // Slice to hold all fetched GroupsService

	visited := make(map[int]bool) // Map to keep track of visited GroupsService

	// fetchGroup fetches subgroups for a single group.
	var fetchGroup func(ctx context.Context, id int)
	fetchGroup = func(ctx context.Context, id int) {
		defer wg.Done() // Signal that the goroutine is done
		mutex.Lock()

		if visited[id] {
			mutex.Unlock()

			return // Skip this group if it has already been visited
		}

		visited[id] = true // Mark this group as visited

		mutex.Unlock()

		opt := &gitlab.ListSubGroupsOptions{ListOptions: gitlab.ListOptions{PerPage: defaultPageSize}}

		for {
			subgroups, resp, err := groupService.ListSubGroups(strconv.Itoa(id), opt, gitlab.WithContext(ctx))
			if err != nil {
				log.Printf("failed to load group %d, error: %s\n", id, err)
				once.Do(func() {
					fetchError = err

					cancel() // Propagate cancellation when the first error occurs
				})

				return
			}

			// Add groups to map
			mutex.Lock()
			for _, subgroup := range subgroups {
				allGroups[subgroup.ID] = domain.NewGroup(subgroup.ID, subgroup.Name, subgroup.FullPath)
			}
			mutex.Unlock()

			for _, subgroup := range subgroups {
				wg.Add(1)

				go func(id int) {
					fetchGroup(ctx, id)
				}(subgroup.ID)
			}

			if resp.CurrentPage >= resp.TotalPages {
				break
			}

			opt.Page = resp.NextPage
		}
	}

	// Start the initial fetch for the given group ID
	wg.Add(1)

	go fetchGroup(ctx, gid)

	wg.Wait() // Wait for all goroutines to finish

	if fetchError != nil {
		return nil, fetchError
	}

	return allGroups, nil
}
