package collect

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

type MembersMockGroupsService struct {
	mock.Mock
}

func (m *MembersMockGroupsService) GetGroup(_ interface{}, _ *gitlab.GetGroupOptions, _ ...gitlab.RequestOptionFunc) (*gitlab.Group, *gitlab.Response, error) {
	panic("not implemented on purpose")
}

func (m *MembersMockGroupsService) ListSubGroups(_ interface{}, _ *gitlab.ListSubGroupsOptions, _ ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	panic("not implemented")
}

func (m *MembersMockGroupsService) ListGroupProjects(_ interface{}, _ *gitlab.ListGroupProjectsOptions, _ ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	panic("not implemented")
}

func (m *MembersMockGroupsService) ListGroupMembers(gid interface{}, opt *gitlab.ListGroupMembersOptions, _ ...gitlab.RequestOptionFunc) ([]*gitlab.GroupMember, *gitlab.Response, error) {
	args := m.Called(gid, opt)

	return args.Get(0).([]*gitlab.GroupMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

// TestFetchAllMembers tests the FetchAllMembers function to ensure it correctly handles various scenarios
// when fetching member data from GitLab GroupsService. This function plays a critical role in ensuring the
// application can reliably retrieve and aggregate member data across potentially multiple GitLab GroupsService.
//
// The test suite is designed to validate the following:
//
// 1. Correct member data retrieval: The function should correctly fetch and organize member data when the GitLab API responds as expected.
// 2. Error handling: The function should properly handle and report errors from the GitLab API without crashing or corrupting data.
// 3. Empty input handling: The function should correctly handle cases where an empty list of GroupsService is provided.
//
// Each test case uses a mock of the GroupsService to simulate interactions with the GitLab API. This allows
// the tests to control the API responses and verify the behavior of FetchAllMembers in a controlled environment,
// ensuring that the function behaves as expected under various conditions without the need to interact with a real GitLab server.
//
// Test cases:
//
//   - "single group with members": Verifies that the function can correctly fetch and map a single group's members.
//     The mock is set up to return a predefined list of members, and the test checks if the result matches the expected output.
//
//   - "handle api error": Ensures that the function can handle API errors gracefully. The mock simulates an API error,
//     and the test verifies that this error is propagated correctly by FetchAllMembers.
//
//   - "empty group list": Checks the function's response to an empty group list input. This test ensures that
//     the function does not attempt any API calls and handles the lack of input gracefully by returning an empty result.
//
// This structure not only tests the functionality but also the resilience and error handling of the FetchAllMembers function.
func TestFetchAllMembers(t *testing.T) {
	// Define test cases
	tests := []struct {
		name        string
		groups      domain.Groups
		mockSetup   func(m *MembersMockGroupsService)
		expected    domain.Users
		expectError bool
	}{
		{
			name:   "single group with members",
			groups: domain.Groups{1: {ID: 1}},
			mockSetup: func(m *MembersMockGroupsService) {
				m.On("ListGroupMembers", "1", mock.Anything).Return([]*gitlab.GroupMember{
					{ID: 1, AccessLevel: gitlab.DeveloperPermissions},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expected: domain.Users{
				1: &domain.User{
					ID:        1,
					Name:      "",
					Username:  "",
					AvatarURL: "",
					Groups: domain.GroupAccessList{domain.GroupAccess{
						Group: domain.Group{ID: 1},
						Role:  domain.Access(gitlab.DeveloperPermissions),
					}},
					Projects: domain.ProjectAccessList{},
				},
			},
		},
		{
			name:   "fetch two pages of members",
			groups: domain.Groups{1: {ID: 1}},
			mockSetup: func(m *MembersMockGroupsService) {
				// First page response for group ID 1
				m.On("ListGroupMembers", "1", mock.MatchedBy(func(opt *gitlab.ListGroupMembersOptions) bool {
					return opt.Page == 0 || opt.Page == 1 // Matches the initial call or first page
				})).Return([]*gitlab.GroupMember{
					{ID: 1, Name: "user1", AccessLevel: gitlab.DeveloperPermissions},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 2, NextPage: 2}, nil)

				// Second page response for group ID 1
				m.On("ListGroupMembers", "1", mock.MatchedBy(func(opt *gitlab.ListGroupMembersOptions) bool {
					return opt.Page == 2 // Matches the second page
				})).Return([]*gitlab.GroupMember{
					{ID: 2, Name: "user2", AccessLevel: gitlab.DeveloperPermissions},
				}, &gitlab.Response{CurrentPage: 2, TotalPages: 2}, nil)
			},
			//expected:    map[int]map[int]gitlab.AccessLevelValue{1: {1: gitlab.DeveloperPermissions}, 2: {1: gitlab.DeveloperPermissions}},
			expected: domain.Users{
				1: &domain.User{ID: 1, Name: "user1", Projects: domain.ProjectAccessList{}, Groups: domain.GroupAccessList{
					domain.GroupAccess{Group: domain.Group{ID: 1, Path: ""}, Role: domain.Access(gitlab.DeveloperPermissions)}},
				},
				2: &domain.User{ID: 2, Name: "user2", Projects: domain.ProjectAccessList{}, Groups: domain.GroupAccessList{
					domain.GroupAccess{Group: domain.Group{ID: 1, Path: ""}, Role: domain.Access(gitlab.DeveloperPermissions)}},
				},
			},
			expectError: false,
		},

		{
			name:   "handle api error",
			groups: domain.Groups{1: {ID: 1}},
			mockSetup: func(m *MembersMockGroupsService) {
				m.On("ListGroupMembers", "1", mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{}, errors.New("api error"))
			},
			expected:    nil,
			expectError: true,
		},
		{
			name:   "empty group list",
			groups: domain.Groups{1: {ID: 1}},
			mockSetup: func(m *MembersMockGroupsService) {
				m.On("ListGroupMembers", "1", mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{}, nil)
			},
			expected: domain.Users{},
		},
	}

	// Execute each test
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			mockService := new(MembersMockGroupsService)
			tc.mockSetup(mockService)

			result, err := fetchGroupMemberships(ctx, mockService, tc.groups)
			if tc.expectError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tc.expected, result)
			}
		})
	}
}
