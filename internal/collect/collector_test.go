package collect

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

type MockGroupsService struct {
	mock.Mock
}

func (m *MockGroupsService) ListSubGroups(gid interface{}, opt *gitlab.ListSubGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	args := m.Called(gid, opt, options)

	return args.Get(0).([]*gitlab.Group), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (m *MockGroupsService) ListGroupMembers(gid interface{}, opt *gitlab.ListGroupMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.GroupMember, *gitlab.Response, error) {
	args := m.Called(gid, opt, options)

	return args.Get(0).([]*gitlab.GroupMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (m *MockGroupsService) ListGroupProjects(gid interface{}, opt *gitlab.ListGroupProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	args := m.Called(gid, opt, options)

	return args.Get(0).([]*gitlab.Project), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (m *MockGroupsService) GetGroup(gid interface{}, opt *gitlab.GetGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Group, *gitlab.Response, error) {
	args := m.Called(gid, opt, options)

	return args.Get(0).(*gitlab.Group), args.Get(1).(*gitlab.Response), args.Error(2)
}

type MockProjectMembersService struct {
	mock.Mock
}

func (m *MockProjectMembersService) ListProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error) {
	args := m.Called(pid, opt, options)

	return args.Get(0).([]*gitlab.ProjectMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (m *MockProjectMembersService) ListAllProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error) {
	args := m.Called(pid, opt, options)

	return args.Get(0).([]*gitlab.ProjectMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

type MockUsersService struct {
	mock.Mock
}

func (m *MockUsersService) GetUser(uid int, opt gitlab.GetUsersOptions, options ...gitlab.RequestOptionFunc) (*gitlab.User, *gitlab.Response, error) {
	args := m.Called(uid, opt, options)

	return args.Get(0).(*gitlab.User), args.Get(1).(*gitlab.Response), args.Error(2)
}

type MockProjectsService struct {
	mock.Mock
}

func (m *MockProjectsService) GetProject(pid interface{}, opt *gitlab.GetProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Project, *gitlab.Response, error) {
	args := m.Called(pid, opt, options)

	return args.Get(0).(*gitlab.Project), args.Get(1).(*gitlab.Response), args.Error(2)
}

func TestGitlabCollector_Collect(t *testing.T) {
	tests := []struct {
		name               string
		setupMocks         func(*MockGroupsService, *MockProjectMembersService, *MockUsersService, *MockProjectsService)
		expectedErr        bool
		expectedCollection domain.Collection
	}{
		{
			name: "Successful data collection",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", 1, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "1", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "John Doe", Username: "johndoe", AccessLevel: gitlab.AccessLevelValue(20)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "2", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "John Doe", Username: "johndoe", AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", "1", mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", "2", mock.Anything, mock.Anything).Return([]*gitlab.Project{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 1, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       1,
					Name:     "John Doe",
					Username: "johndoe",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{ID: 1, Path: "group1"}, Role: domain.Access(gitlab.ReporterPermissions)},
						{Group: domain.Group{ID: 2, Path: "group1/group2"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group1/project"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
				},
			},
		},
		{
			name: "Successful data collection",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", 1, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "1", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "John Doe", Username: "johndoe", AccessLevel: gitlab.AccessLevelValue(20)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "2", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "John Doe", Username: "johndoe", AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       1,
					Name:     "John Doe",
					Username: "johndoe",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{ID: 1, Path: "group1"}, Role: domain.Access(gitlab.ReporterPermissions)},
						{Group: domain.Group{ID: 2, Path: "group1/group2"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group1/project"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
				},
			},
		},
		{
			name: "Successful data collection with ignored projects",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", 1, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "1", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "2", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "John Doe", Username: "johndoe", AccessLevel: gitlab.AccessLevelValue(50)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "this-is-not-in-the-root-group/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       1,
					Name:     "John Doe",
					Username: "johndoe",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{ID: 2, Path: "group1/group2"}, Role: domain.Access(gitlab.OwnerPermissions)},
					},
					Projects: domain.ProjectAccessList{},
				},
			},
		},
		{
			name: "Users with no access are not included",
			setupMocks: func(groups *MockGroupsService, projMembers *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", 1, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				groups.On("ListSubGroups", "1", mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", "2", mock.Anything, mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				groups.On("ListGroupMembers", "1", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "2", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				groups.On("ListGroupProjects", "1", mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "this-is-not-in-the-root-group/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", "2", mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 2, PathWithNamespace: "group1/group2/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				projMembers.On("ListProjectMembers", 1, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "user1", Username: "user1", AccessLevel: gitlab.OwnerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projMembers.On("ListProjectMembers", 2, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 2, Name: "user2", Username: "user2", AccessLevel: gitlab.OwnerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       2,
					Name:     "user2",
					Username: "user2",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 2, Path: "group1/group2/project"}, Role: domain.Access(gitlab.OwnerPermissions)},
					},
				},
			},
		},
		{
			name: "Error when fetching group details",
			setupMocks: func(groups *MockGroupsService, _ *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{}, &gitlab.Response{}, errors.New("simulated error while fetching group details"))
			},
			expectedErr:        true,
			expectedCollection: domain.Collection{},
		},
		{
			name: "Error when fetching subgroup details",
			setupMocks: func(groups *MockGroupsService, _ *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil).Once()
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, errors.New("simulated error while fetching group details"))
			},
			expectedErr:        true,
			expectedCollection: domain.Collection{},
		},
		{
			name: "Error when fetching project details",
			setupMocks: func(groups *MockGroupsService, _ *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{Name: "penguins"}, &gitlab.Response{}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, errors.New("simulated error while fetching project details"))
			},
			expectedErr:        true,
			expectedCollection: domain.Collection{},
		},

		{
			name: "Projects are sorted",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group/death-clock"}, {ID: 2, PathWithNamespace: "group/doomsday-device"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 1, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "Dr. Hubert J. Farnsworth", Username: "professor", AccessLevel: gitlab.AccessLevelValue(40)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 2, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "Dr. Hubert J. Farnsworth", Username: "professor", AccessLevel: gitlab.AccessLevelValue(50)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       1,
					Name:     "Dr. Hubert J. Farnsworth",
					Username: "professor",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group/death-clock"}, Role: domain.Access(gitlab.MaintainerPermissions)},
						{Project: &domain.Project{ID: 2, Path: "group/doomsday-device"}, Role: domain.Access(gitlab.OwnerPermissions)},
					},
				},
			},
		},
		{
			name: "Groups are sorted",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group1"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil).Once()
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 2, FullPath: "group1/group2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "1", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "Leela", Username: "leela", AccessLevel: gitlab.AccessLevelValue(20)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", "2", mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, Name: "Leela", Username: "leela", AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", "1", mock.Anything, mock.Anything).Return([]*gitlab.Project{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", "2", mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/group2/company"}, {ID: 2, PathWithNamespace: "group1/group2/ship"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 1, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(20)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 2, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(40)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:   1,
					Name: "Leela", Username: "leela",
					Groups: domain.GroupAccessList{
						{Group: domain.Group{ID: 1, Path: "group1"}, Role: domain.Access(gitlab.ReporterPermissions)},
						{Group: domain.Group{ID: 2, Path: "group1/group2"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group1/group2/company"}, Role: domain.Access(gitlab.ReporterPermissions)},
						{Project: &domain.Project{ID: 2, Path: "group1/group2/ship"}, Role: domain.Access(gitlab.MaintainerPermissions)},
					},
				},
			},
		},
		{
			name: "Project only members",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group/project"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				projects.On("ListProjectMembers", 1, mock.Anything, mock.Anything).Return([]*gitlab.ProjectMember{
					{ID: 1, Name: "Bender", Username: "bender", AccessLevel: gitlab.AccessLevelValue(20)},
					{ID: 2, Name: "Fry", Username: "fry", AccessLevel: gitlab.AccessLevelValue(30)},
					{ID: 3, Name: "Zoidberg", Username: "zoidberg", AccessLevel: gitlab.AccessLevelValue(40)},
					{ID: 4, Name: "Amy", Username: "amy", AccessLevel: gitlab.AccessLevelValue(50)},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedErr: false,
			expectedCollection: domain.Collection{
				{
					ID:       4,
					Name:     "Amy",
					Username: "amy",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group/project"}, Role: domain.Access(gitlab.OwnerPermissions)},
					},
				},

				{
					ID:       1,
					Name:     "Bender",
					Username: "bender",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group/project"}, Role: domain.Access(gitlab.ReporterPermissions)},
					},
				},
				{
					ID:       2,
					Name:     "Fry",
					Username: "fry",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group/project"}, Role: domain.Access(gitlab.DeveloperPermissions)},
					},
				},
				{
					ID:       3,
					Name:     "Zoidberg",
					Username: "zoidberg",
					Groups:   domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						{Project: &domain.Project{ID: 1, Path: "group/project"}, Role: domain.Access(gitlab.MaintainerPermissions)},
					},
				},
			},
		},
		{
			name: "Error fetching groups",
			setupMocks: func(groups *MockGroupsService, _ *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{}, &gitlab.Response{}, errors.New("simulated error while fetching groups"))
			},
			expectedErr: true,
		},
		{
			name: "Error fetching members",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, users *MockUsersService, projectsService *MockProjectsService) {
				_ = projectsService
				_ = users
				_ = projects
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{}, errors.New("error fetching members"))
			},
			expectedErr: true,
		},
		{
			name: "Error fetching projects",
			setupMocks: func(groups *MockGroupsService, projects *MockProjectMembersService, users *MockUsersService, projectsService *MockProjectsService) {
				_ = projectsService
				_ = users
				_ = projects

				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{{ID: 1, AccessLevel: gitlab.AccessLevelValue(30)}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupProjects", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Project{}, &gitlab.Response{}, errors.New("error fetching projects"))
			},
			expectedErr: true,
		},
		{
			name: "Error fetching user details",
			setupMocks: func(groups *MockGroupsService, _ *MockProjectMembersService, _ *MockUsersService, _ *MockProjectsService) {
				groups.On("GetGroup", mock.Anything, mock.Anything, mock.Anything).Return(&gitlab.Group{ID: 1, FullPath: "group"}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListSubGroups", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.Group{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				groups.On("ListGroupMembers", mock.Anything, mock.Anything, mock.Anything).Return([]*gitlab.GroupMember{}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, errors.New("error fetching user details"))
			},
			expectedErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			groupsService := new(MockGroupsService)
			projectMembersService := new(MockProjectMembersService)
			mockUsers := new(MockUsersService)
			mockProjectsService := new(MockProjectsService)

			tt.setupMocks(groupsService, projectMembersService, mockUsers, mockProjectsService)

			// Initialize the GitlabCollector with mocked services
			groupID := 1
			collector, err := NewGitlabCollector(&groupID, groupsService, projectMembersService)
			require.NoError(t, err)

			// Call the Collect method
			collection, err := collector.Collect(context.Background())

			if tt.expectedErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expectedCollection, collection)
			}

			groupsService.AssertExpectations(t)
			projectMembersService.AssertExpectations(t)
			mockUsers.AssertExpectations(t)
			mockProjectsService.AssertExpectations(t)
		})
	}
}
