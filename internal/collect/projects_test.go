package collect

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

type ProjectsMockProjectMembersService struct {
	mock.Mock
}

func (s *ProjectsMockProjectMembersService) ListAllProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error) {
	_ = options

	args := s.Called(pid, opt)

	return args.Get(0).([]*gitlab.ProjectMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (s *ProjectsMockProjectMembersService) ListProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error) {
	_ = options

	args := s.Called(pid, opt)

	return args.Get(0).([]*gitlab.ProjectMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

type ProjectsMockGroupsService struct {
	mock.Mock
}

func (s *ProjectsMockGroupsService) GetGroup(_ interface{}, _ *gitlab.GetGroupOptions, _ ...gitlab.RequestOptionFunc) (*gitlab.Group, *gitlab.Response, error) {
	panic("not implemented on purpose")
}

func (s *ProjectsMockGroupsService) ListSubGroups(gid interface{}, opt *gitlab.ListSubGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	_ = gid
	_ = opt
	_ = options

	panic("not implemented")
}

func (s *ProjectsMockGroupsService) ListGroupProjects(gid interface{}, opt *gitlab.ListGroupProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	_ = options
	args := s.Called(gid, opt)

	return args.Get(0).([]*gitlab.Project), args.Get(1).(*gitlab.Response), args.Error(2)
}

func (s *ProjectsMockGroupsService) ListGroupMembers(gid interface{}, opt *gitlab.ListGroupMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.GroupMember, *gitlab.Response, error) {
	_ = options
	args := s.Called(gid, opt)

	return args.Get(0).([]*gitlab.GroupMember), args.Get(1).(*gitlab.Response), args.Error(2)
}

func TestFetchAllProjects(t *testing.T) {
	tests := []struct {
		name           string
		groups         domain.Groups
		setupMocks     func(*ProjectsMockGroupsService, *ProjectsMockProjectMembersService)
		expectedResult domain.Users
		expectError    bool
	}{
		{
			name:   "single group with single project",
			groups: domain.Groups{1: domain.Group{ID: 1, Path: "group1"}},
			setupMocks: func(mg *ProjectsMockGroupsService, mp *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/project1"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 1, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "batman", AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedResult: domain.Users{
				1: &domain.User{
					ID:     1,
					Name:   "batman",
					Groups: domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID:   1,
								Path: "group1/project1",
							},
							Role: domain.Access(gitlab.DeveloperPermissions),
						},
					},
				},
			},
			expectError: false,
		},
		{
			name:   "fetch two pages of projects",
			groups: domain.Groups{1000: {ID: 1000}},
			setupMocks: func(groupService *ProjectsMockGroupsService, memberService *ProjectsMockProjectMembersService) {
				// First page response for group ID 1
				groupService.On("ListGroupProjects", "1000", mock.MatchedBy(func(opt *gitlab.ListGroupProjectsOptions) bool {
					return opt.Page == 0 || opt.Page == 1 // Matches the initial call or first page
				})).Return([]*gitlab.Project{
					{ID: 2000},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 2, NextPage: 2}, nil)

				// Second page response for group ID 1
				groupService.On("ListGroupProjects", "1000", mock.MatchedBy(func(opt *gitlab.ListGroupProjectsOptions) bool {
					return opt.Page == 2 // Matches the second page
				})).Return([]*gitlab.Project{
					{ID: 3000},
				}, &gitlab.Response{CurrentPage: 2, TotalPages: 2}, nil)

				// Members for project ID 2 and ID 3 (no further members)
				memberService.On("ListProjectMembers", 2000, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1000, AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				memberService.On("ListProjectMembers", 3000, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1000, AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedResult: domain.Users{
				1000: &domain.User{
					ID:        1000,
					Name:      "",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{ID: 2000},
							Role:    domain.Access(gitlab.DeveloperPermissions),
						},
						domain.ProjectAccess{
							Project: &domain.Project{ID: 3000},
							Role:    domain.Access(gitlab.DeveloperPermissions),
						},
					},
				},
			},
			expectError: false,
		},
		{
			name:   "fetch two pages of members",
			groups: domain.Groups{1000: {ID: 1000}},
			setupMocks: func(groupService *ProjectsMockGroupsService, memberService *ProjectsMockProjectMembersService) {
				// Mock response for group projects
				groupService.On("ListGroupProjects", "1000", mock.MatchedBy(func(opt *gitlab.ListGroupProjectsOptions) bool {
					return opt.Page == 0 || opt.Page == 1 // Matches the initial call or first page
				})).Return([]*gitlab.Project{
					{ID: 7},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)

				// First page response for project members
				memberService.On("ListProjectMembers", 7, mock.MatchedBy(func(opt *gitlab.ListProjectMembersOptions) bool {
					return opt.Page == 0 || opt.Page == 1 // Matches the initial call or first page
				})).Return([]*gitlab.ProjectMember{
					{ID: 1000, Name: "batman", AccessLevel: gitlab.DeveloperPermissions},
				}, &gitlab.Response{CurrentPage: 1, TotalPages: 2, NextPage: 2}, nil)

				// Second page response for project members
				memberService.On("ListProjectMembers", 7, mock.MatchedBy(func(opt *gitlab.ListProjectMembersOptions) bool {
					return opt.Page == 2 // Matches the second page
				})).Return([]*gitlab.ProjectMember{
					{ID: 2000, Name: "robin", AccessLevel: gitlab.MaintainerPermissions},
				}, &gitlab.Response{CurrentPage: 2, TotalPages: 2}, nil)
			},
			expectedResult: domain.Users{
				1000: &domain.User{
					ID:        1000,
					Name:      "batman",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID: 7,
							},
							Role: domain.Access(gitlab.DeveloperPermissions),
						},
					},
				},
				2000: &domain.User{
					ID:        2000,
					Name:      "robin",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID: 7,
							},
							Role: domain.Access(gitlab.MaintainerPermissions),
						},
					},
				},
			},
			expectError: false,
		},
		{
			name:   "error on second page of project members",
			groups: domain.Groups{1: {ID: 1}},
			setupMocks: func(mg *ProjectsMockGroupsService, mp *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				// First page response for project members
				mp.On("ListProjectMembers", 1, mock.MatchedBy(func(opt *gitlab.ListProjectMembersOptions) bool {
					return opt.Page == 0 || opt.Page == 1
				})).Return([]*gitlab.ProjectMember{{ID: 1, AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 2, NextPage: 2}, nil)
				// Second page response for project members with error
				mp.On("ListProjectMembers", 1, mock.MatchedBy(func(opt *gitlab.ListProjectMembersOptions) bool {
					return opt.Page == 2
				})).Return([]*gitlab.ProjectMember{}, &gitlab.Response{}, errors.New("fetch error on second page"))
			},
			expectedResult: nil,
			expectError:    true,
		},
		{
			name:   "API error on fetching projects",
			groups: domain.Groups{1: {ID: 1}},
			setupMocks: func(mg *ProjectsMockGroupsService, _ *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{}, &gitlab.Response{}, errors.New("api error"))
			},
			expectedResult: nil,
			expectError:    true,
		},
		{
			name:   "multiple groups, single project each",
			groups: domain.Groups{1: {ID: 1}, 2: {ID: 2}},
			setupMocks: func(mg *ProjectsMockGroupsService, mp *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{{ID: 1}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mg.On("ListGroupProjects", "2", mock.Anything).Return([]*gitlab.Project{{ID: 2}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 1, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "batman", AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 2, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 2, Name: "robin", AccessLevel: gitlab.MaintainerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedResult: domain.Users{
				1: &domain.User{
					ID:        1,
					Name:      "batman",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID: 1,
							},
							Role: domain.Access(gitlab.DeveloperPermissions),
						},
					},
				},
				2: &domain.User{
					ID:        2,
					Name:      "robin",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID: 2,
							},
							Role: domain.Access(gitlab.MaintainerPermissions),
						},
					},
				},
			},
			expectError: false,
		},
		{
			name:   "single group with multiple projects",
			groups: domain.Groups{1: {ID: 1}},
			setupMocks: func(mg *ProjectsMockGroupsService, mp *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/project1"}, {ID: 2, PathWithNamespace: "group1/project2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 1, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "PO", AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 2, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "PO", AccessLevel: gitlab.MaintainerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedResult: domain.Users{
				1: &domain.User{
					ID:        1,
					Name:      "PO",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{
								ID:   1,
								Name: "",
								Path: "group1/project1",
							},
							Role: domain.Access(gitlab.DeveloperPermissions),
						},
						domain.ProjectAccess{
							Project: &domain.Project{
								ID:   2,
								Name: "",
								Path: "group1/project2",
							},
							Role: domain.Access(gitlab.MaintainerPermissions),
						},
					},
				},
			},
			expectError: false,
		},
		{
			name:   "multiple groups with multiple projects each",
			groups: domain.Groups{1: {ID: 1, Path: "group1"}, 2: {ID: 2, Path: "group2"}},
			setupMocks: func(mg *ProjectsMockGroupsService, mp *ProjectsMockProjectMembersService) {
				mg.On("ListGroupProjects", "1", mock.Anything).Return([]*gitlab.Project{{ID: 1, PathWithNamespace: "group1/project1"}, {ID: 2, PathWithNamespace: "group1/project2"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mg.On("ListGroupProjects", "2", mock.Anything).Return([]*gitlab.Project{{ID: 3, PathWithNamespace: "group1/project3"}, {ID: 4, PathWithNamespace: "group1/project4"}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 1, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "Amy", AccessLevel: gitlab.DeveloperPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 2, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "Amy", AccessLevel: gitlab.MaintainerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 3, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 1, Name: "Amy", AccessLevel: gitlab.OwnerPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
				mp.On("ListProjectMembers", 4, mock.Anything).Return([]*gitlab.ProjectMember{{ID: 2, Name: "Fry", AccessLevel: gitlab.ReporterPermissions}}, &gitlab.Response{CurrentPage: 1, TotalPages: 1}, nil)
			},
			expectedResult: domain.Users{
				1: &domain.User{
					ID:        1,
					Name:      "Amy",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{ID: 1, Path: "group1/project1"},
							Role:    domain.Access(gitlab.DeveloperPermissions),
						},
						domain.ProjectAccess{
							Project: &domain.Project{ID: 2, Path: "group1/project2"},
							Role:    domain.Access(gitlab.MaintainerPermissions),
						},
						domain.ProjectAccess{
							Project: &domain.Project{ID: 3, Path: "group1/project3"},
							Role:    domain.Access(gitlab.OwnerPermissions),
						},
					},
				},
				2: &domain.User{
					ID:        2,
					Name:      "Fry",
					Username:  "",
					AvatarURL: "",
					Groups:    domain.GroupAccessList{},
					Projects: domain.ProjectAccessList{
						domain.ProjectAccess{
							Project: &domain.Project{ID: 4, Path: "group1/project4"},
							Role:    domain.Access(gitlab.ReporterPermissions),
						},
					},
				},
			},
			expectError: false,
		},
	}

	for _, tt := range tests {
		testCase := tt
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.Background()
			mockGroupsService := new(ProjectsMockGroupsService)
			mockProjectMembersService := new(ProjectsMockProjectMembersService)
			testCase.setupMocks(mockGroupsService, mockProjectMembersService)
			result, err := fetchProjectMemberships(ctx, make(domain.Users), mockGroupsService, mockProjectMembersService, testCase.groups)

			if testCase.expectError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, testCase.expectedResult, result)
			}
		})
	}
}
