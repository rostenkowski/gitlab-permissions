package collect

import (
	"context"
	"log"
	"strconv"
	"sync"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// fetchGroupMemberships retrieves all members of given GroupsService concurrently.
func fetchGroupMemberships(ctx context.Context, service domain.GroupsService, groups domain.Groups) (domain.Users, error) {
	var (
		wg         sync.WaitGroup // WaitGroup to synchronize goroutines
		mutex      sync.Mutex     // Mutex to protect shared members map
		fetchError error          // Variable to store any error encountered during fetching
	)

	members := make(domain.Users)

	// fetchMembers fetches members for a single group.
	fetchMembers := func(ctx context.Context, group domain.Group) {
		defer wg.Done() // Signal that the goroutine is done

		opt := &gitlab.ListGroupMembersOptions{ListOptions: gitlab.ListOptions{PerPage: defaultPageSize}}

		for {
			groupMembers, resp, err := service.ListGroupMembers(strconv.Itoa(group.ID), opt, gitlab.WithContext(ctx))
			if err != nil {
				mutex.Lock()
				fetchError = err
				mutex.Unlock()

				return
			}

			mutex.Lock()
			for _, m := range groupMembers {
				if _, ok := members[m.ID]; !ok {
					// Creating new user
					log.Println("creating group member:", m.Username, group.Path)
					members[m.ID] = domain.NewUser(m.ID, m.Name, m.Username, m.AvatarURL)
				}

				members[m.ID].SetGroupAccess(group, domain.Access(m.AccessLevel))
			}
			mutex.Unlock()

			if resp.CurrentPage >= resp.TotalPages {
				break
			}

			opt.Page = resp.NextPage
		}
	}

	// Start a goroutine for each group to fetch its members
	for _, group := range groups {
		wg.Add(1)

		go fetchMembers(ctx, group)
	}

	wg.Wait() // Wait for all goroutines to finish

	return members, fetchError
}
