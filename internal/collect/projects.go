package collect

import (
	"context"
	"log"
	"strconv"
	"sync"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// fetchProjectMemberships retrieves all projects of a given list of GroupsService and the members within those projects concurrently.
func fetchProjectMemberships(ctx context.Context, users domain.Users, groupService domain.GroupsService, memberService domain.ProjectMembersService, groups domain.Groups) (domain.Users, error) {
	var wg sync.WaitGroup // WaitGroup to synchronize goroutines

	var (
		mutex      sync.Mutex // Mutex to protect shared projects map
		fetchError error      // Variable to store any error encountered during fetching
		once       sync.Once  // Ensure error is set only once
	)

	// fetchGroupProjects fetches projects for a single group.
	fetchGroupProjects := func(ctx context.Context, group domain.Group) {
		defer wg.Done()

		opt := &gitlab.ListGroupProjectsOptions{ListOptions: gitlab.ListOptions{PerPage: defaultPageSize}}

		for {
			groupProjects, resp, err := groupService.ListGroupProjects(strconv.Itoa(group.ID), opt, gitlab.WithContext(ctx))
			if err != nil {
				once.Do(func() {
					fetchError = err
				})

				return
			}

			for _, project := range groupProjects {
				projMembersOpt := &gitlab.ListProjectMembersOptions{ListOptions: gitlab.ListOptions{PerPage: defaultPageSize}}

				for {
					projMembers, memResp, err := memberService.ListProjectMembers(project.ID, projMembersOpt, gitlab.WithContext(ctx))
					if err != nil {
						once.Do(func() {
							fetchError = err
						})

						return
					}

					mutex.Lock()
					for _, m := range projMembers {
						if _, exists := users[m.ID]; !exists {
							log.Println("creating project member:", m.Username, project.PathWithNamespace)

							users[m.ID] = domain.NewUser(m.ID, m.Name, m.Username, m.AvatarURL)
						}

						users[m.ID].SetProjectAccess(domain.NewProject(project.ID, project.Name, project.PathWithNamespace), domain.Access(m.AccessLevel))
					}
					mutex.Unlock()

					if memResp.CurrentPage >= memResp.TotalPages {
						break
					}

					projMembersOpt.Page = memResp.NextPage
				}
			}

			if resp.CurrentPage >= resp.TotalPages {
				break
			}

			opt.Page = resp.NextPage
		}
	}

	// Start a goroutine for each group to fetch its projects
	for _, group := range groups {
		wg.Add(1)

		go fetchGroupProjects(ctx, group)
	}

	wg.Wait() // Wait for all goroutines to finish

	return users, fetchError
}
