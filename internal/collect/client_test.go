package collect

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

// MockTransportFactory is a mock for TransportFactory.
type MockTransportFactory struct {
	mock.Mock
}

func (m *MockTransportFactory) NewCustomTransport(cacheDir string, ttl time.Duration) (http.RoundTripper, error) {
	args := m.Called(cacheDir, ttl)
	rt, ok := args.Get(0).(http.RoundTripper)

	if !ok && args.Get(0) != nil {
		panic("expected http.RoundTripper type")
	}

	return rt, args.Error(1)
}

func TestNewGitLabClient(t *testing.T) {
	accessToken := "test-token"
	cacheDir := "/test/cache"
	cacheTTL := 60

	t.Run("successful client creation", func(t *testing.T) {
		mockFactory := new(MockTransportFactory)
		mockFactory.On("NewCustomTransport", cacheDir, time.Duration(cacheTTL)*time.Second).Return(&http.Transport{}, nil)
		client, err := NewGitLabClient(accessToken, cacheDir, cacheTTL, mockFactory)

		require.NoError(t, err)
		assert.NotNil(t, client)
		mockFactory.AssertExpectations(t)
	})

	t.Run("error creating transport", func(t *testing.T) {
		mockFactory := new(MockTransportFactory)
		mockFactory.On("NewCustomTransport", cacheDir, time.Duration(cacheTTL)*time.Second).Return(nil, errors.New("transport creation failed"))
		client, err := NewGitLabClient(accessToken, cacheDir, cacheTTL, mockFactory)

		require.Error(t, err)
		assert.Nil(t, client)
		assert.Contains(t, err.Error(), "failed to create transport: transport creation failed")
		mockFactory.AssertExpectations(t)
	})
}

func TestNewCustomTransport(t *testing.T) {
	tests := []struct {
		name        string
		setupDir    func() string // Use a setup function to defer directory creation
		duration    time.Duration
		expectError bool
	}{
		{
			name: "valid directory",
			setupDir: func() string {
				dir, err := os.MkdirTemp("", "cache")
				if err != nil {
					t.Fatal("failed to create temp dir:", err)
				}

				return dir
			},
			duration:    10 * time.Second,
			expectError: false,
		},
		{
			name: "invalid directory",
			setupDir: func() string {
				return ""
			},
			duration:    10 * time.Second,
			expectError: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cacheDir := tt.setupDir() // Set up the directory when the test runs

			// Ensure cleanup
			defer func() {
				_ = os.RemoveAll(cacheDir)
			}()

			f := &CustomTransportFactory{}
			if _, err := f.NewCustomTransport(cacheDir, tt.duration); (err != nil) != tt.expectError {
				t.Errorf("Test %s failed, expected error: %v, got: %v", tt.name, tt.expectError, err)
			}
		})
	}
}

func TestCustomTransport_RoundTrip(t *testing.T) {
	tests := []struct {
		name        string
		statusCode  int
		expectCache bool
	}{
		{
			name:        "cacheable status",
			statusCode:  200,
			expectCache: true,
		},
		{
			name:        "non-cacheable status",
			statusCode:  404,
			expectCache: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
				w.WriteHeader(tt.statusCode)
			}))
			defer server.Close()

			client := server.Client()
			ctx := context.Background()
			req, _ := http.NewRequestWithContext(ctx, http.MethodGet, server.URL, http.NoBody)
			transport := &CustomTransport{
				Transport:     client.Transport,
				CacheDuration: 10 * time.Second,
			}
			client.Transport = transport

			resp, err := client.Do(req)
			if err != nil {
				t.Fatal("failed to make request:", err)
			}

			defer func() {
				_ = resp.Body.Close()
			}()

			// Check if caching logic is correctly applied based on status code
			if (resp.Header.Get("Cache-Control") == "") == tt.expectCache {
				t.Errorf("Cache-Control headers do not match expected caching behavior")
			}
		})
	}
}

// roundTripperFunc type adapts a function to the http.RoundTripper interface.
type roundTripperFunc func(*http.Request) (*http.Response, error)

// RoundTrip executes a single HTTP transaction.
func (f roundTripperFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req)
}

func TestCustomTransport_RoundTrip_Error(t *testing.T) {
	// Create a custom RoundTripper that always returns an error
	failingTransport := roundTripperFunc(func(_ *http.Request) (*http.Response, error) {
		return nil, errors.New("simulated network error")
	})

	// Set up the CustomTransport with the failing RoundTripper
	transport := &CustomTransport{
		Transport:     failingTransport, // Use this custom failing transport
		CacheDuration: 10 * time.Second,
	}

	// Create an HTTP client and set its transport to the custom one
	client := &http.Client{
		Transport: transport,
	}

	// Create a new request
	ctx := context.Background()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://example.com", http.NoBody)
	if err != nil {
		t.Fatal("failed to create request:", err)
	}

	// Perform the request
	r, err := client.Do(req)
	if err == nil {
		t.Error("expected an error but got none")
	}

	defer func() {
		// It's nobody
		if r != nil && r.Body != nil {
			_ = r.Body.Close()
		}
	}()
}
