package collect

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gregjones/httpcache"
	"github.com/gregjones/httpcache/diskcache"
	"github.com/xanzy/go-gitlab"
)

// CacheDirectoryMode is the default permissions for the cache directory.
const CacheDirectoryMode = 0o750

// TransportFactory defines an interface for creating new HTTP transports.
type TransportFactory interface {
	// NewCustomTransport creates new custom transport with caching capabilities.
	NewCustomTransport(cacheDir string, ttl time.Duration) (http.RoundTripper, error)
}

// NewGitLabClient creates a configured *gitlab.Client instance.
func NewGitLabClient(accessToken, cacheDir string, cacheTTL int, factory TransportFactory) (*gitlab.Client, error) {
	// Configure the custom HTTP transport
	customTransport, err := factory.NewCustomTransport(cacheDir, time.Duration(cacheTTL)*time.Second)
	if err != nil {
		return nil, fmt.Errorf("failed to create transport: %w", err)
	}

	httpClient := &http.Client{
		Transport: customTransport,
	}

	// Create the GitLab client
	return gitlab.NewClient(accessToken, gitlab.WithHTTPClient(httpClient))
}

// CustomTransportFactory is a factory for creating custom HTTP transports.
type CustomTransportFactory struct{}

// NewCustomTransport creates a new instance of CustomTransport with a disk-based cache.
func (f *CustomTransportFactory) NewCustomTransport(cacheDir string, duration time.Duration) (http.RoundTripper, error) {
	// Check if the cache directory exists or create it if it does not exist
	if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
		if err := os.MkdirAll(cacheDir, CacheDirectoryMode); err != nil {
			return nil, fmt.Errorf("failed to create cache directory: %w", err)
		}
	}

	store := diskcache.New(cacheDir) // Using diskcache for storage
	transport := httpcache.NewTransport(store)

	return &CustomTransport{
		Transport:     transport,
		CacheDuration: duration,
	}, nil
}

// CustomTransport is an HTTP transport that adds caching functionality.
type CustomTransport struct {
	Transport     http.RoundTripper // The underlying transport
	CacheDuration time.Duration     // Duration to cache responses
}

// RoundTrip executes a single HTTP transaction, adding caching functionality.
func (c *CustomTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := c.Transport.RoundTrip(req)
	if err != nil {
		log.Printf("HTTP request failed: %v\n", err)

		return nil, fmt.Errorf("HTTP request failed: %w", err)
	}

	// Cache successful responses
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		maxAge := int(c.CacheDuration.Seconds())
		resp.Header.Set("Cache-Control", "public, max-age="+strconv.Itoa(maxAge))
	} else {
		log.Printf("received non-cacheable status: %d\n", resp.StatusCode)
	}

	return resp, nil
}
