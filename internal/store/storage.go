package store

import (
	"sync"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// Storage defines an interface for storing and retrieving data.
type Storage interface {
	// GetData retrieves the stored collection of data.
	GetData() domain.Collection
	// SetData stores the given collection of data.
	SetData(snap domain.Collection)
	// Ready checks if the storage has been initialized with data.
	Ready() bool
}

// NewMemoryStorage creates a new instance of MemoryStorage.
func NewMemoryStorage(initialized bool, currentData domain.Collection) *MemoryStorage {
	return &MemoryStorage{
		lock:        sync.RWMutex{},
		currentData: currentData,
		initialized: initialized,
	}
}

// MemoryStorage provides an in-memory implementation of the Storage interface.
type MemoryStorage struct {
	lock        sync.RWMutex      // RWMutex to ensure thread-safe access to currentData
	currentData domain.Collection // Collection of current data
	initialized bool              // Flag indicating if the storage has been initialized with data
}

// GetData retrieves the stored collection of data.
func (s *MemoryStorage) GetData() domain.Collection {
	s.lock.RLock()

	defer s.lock.RUnlock()

	return s.currentData
}

// SetData stores the given collection of data and marks the storage as initialized.
func (s *MemoryStorage) SetData(snap domain.Collection) {
	s.lock.Lock()

	defer s.lock.Unlock()

	s.currentData = snap
	s.initialized = true
}

// Ready checks if the storage has been initialized with data.
func (s *MemoryStorage) Ready() bool {
	s.lock.RLock()

	defer s.lock.RUnlock()

	return s.initialized
}
