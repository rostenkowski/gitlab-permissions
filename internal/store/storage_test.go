package store

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/domain"
)

// TestMemoryStorage_GetData is a table-driven test for the GetData method of the App struct.
func TestMemoryStorage_GetData(t *testing.T) {
	// Define the test cases
	tests := []struct {
		name         string
		initialData  domain.Collection
		expectedData domain.Collection
	}{
		{
			name:         "Empty data",
			initialData:  domain.Collection{},
			expectedData: domain.Collection{},
		},
		{
			name: "Non-empty data",
			initialData: domain.Collection{
				{Name: "User1", Username: "user1"},
				{Name: "User2", Username: "user2"},
			},
			expectedData: domain.Collection{
				{Name: "User1", Username: "user1"},
				{Name: "User2", Username: "user2"},
			},
		},
	}

	// Iterate through the test cases
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Initialize the App struct with the initial data
			storage := &MemoryStorage{
				lock:        sync.RWMutex{},
				currentData: tt.initialData,
				initialized: true,
			}

			// Call the GetData method
			got := storage.GetData()

			// Check if the returned data matches the expected data
			if !reflect.DeepEqual(got, tt.expectedData) {
				t.Errorf("GetData() = %v, want %v", got, tt.expectedData)
			}
		})
	}
}

// TestMemoryStorage_SetData is a table-driven test for the SetData method of the MemoryStorage struct.
func TestMemoryStorage_SetData(t *testing.T) {
	// Define the test cases
	tests := []struct {
		name         string
		initialData  domain.Collection
		newData      domain.Collection
		expectedData domain.Collection
		expectedInit bool
	}{
		{
			name: "Set empty data",
			initialData: domain.Collection{
				{Name: "User1", Username: "user1"},
			},
			newData:      domain.Collection{},
			expectedData: domain.Collection{},
			expectedInit: true,
		},
		{
			name:        "Set non-empty data",
			initialData: domain.Collection{},
			newData: domain.Collection{
				{Name: "User2", Username: "user2"},
				{Name: "User3", Username: "user3"},
			},
			expectedData: domain.Collection{
				{Name: "User2", Username: "user2"},
				{Name: "User3", Username: "user3"},
			},
			expectedInit: true,
		},
	}

	// Iterate through the test cases
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Initialize the App struct with the initial data
			storage := &MemoryStorage{
				lock:        sync.RWMutex{},
				currentData: tt.initialData,
				initialized: true,
			}

			// Call the SetData method
			storage.SetData(tt.newData)

			// Check if the current data matches the expected data
			if !reflect.DeepEqual(storage.GetData(), tt.expectedData) {
				t.Errorf("currentData = %v, want %v", storage.GetData(), tt.expectedData)
			}

			// Check if the initialized flag is set correctly
			if storage.Ready() != tt.expectedInit {
				t.Errorf("initialized = %v, want %v", storage.Ready(), tt.expectedInit)
			}
		})
	}
}

// TestMemoryStorage_Ready is a table-driven test for the Ready method of the MemoryStorage struct.
func TestMemoryStorage_Ready(t *testing.T) {
	t.Parallel()

	// Define the test cases
	tests := []struct {
		name          string
		initialState  bool
		expectedReady bool
	}{
		{
			name:          "Not ready",
			initialState:  false,
			expectedReady: false,
		},
		{
			name:          "Ready",
			initialState:  true,
			expectedReady: true,
		},
	}

	// Iterate through the test cases
	for _, tt := range tests {
		testCase := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			// Initialize the App struct with the initial state
			storage := &MemoryStorage{
				lock:        sync.RWMutex{},
				currentData: nil,
				initialized: testCase.initialState,
			}

			// Call the Ready method
			got := storage.Ready()

			// Check if the returned readiness state matches the expected state
			if got != testCase.expectedReady {
				t.Errorf("Ready() = %v, want %v", got, testCase.expectedReady)
			}
		})
	}
}

// TestNewMemoryStorage tests the NewMemoryStorage function.
func TestNewMemoryStorage(t *testing.T) {
	storage := NewMemoryStorage(false, domain.Collection{})

	if storage == nil {
		t.Fatal("expected non-nil MemoryStorage")
	}

	// Verify the initial state of the storage
	if storage.initialized != false {
		t.Errorf("expected initialized to be false, got %v", storage.initialized)
	}

	if len(storage.currentData) != 0 {
		t.Errorf("expected currentData to be empty slice, got %v", storage.currentData)
	}

	// Instead of checking the lock directly, ensure that the storage methods work correctly
	// Verify that the lock works correctly by calling the methods
	data := storage.GetData()
	if len(data) != 0 {
		t.Errorf("expected GetData to return empty slice, got %v", data)
	}

	storage.SetData(nil)

	if !storage.Ready() {
		t.Errorf("expected storage to be ready after SetData, got %v", storage.Ready())
	}
}
