package domain

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func TestGroupAccess_String(t *testing.T) {
	perm := &GroupAccess{
		Group: Group{Path: "developers"},
		Role:  Access(gitlab.MaintainerPermissions),
	}
	expected := "developers (Maintainer)"
	assert.Equal(t, expected, perm.String(), "GroupPermission String method should format correctly")
}

func TestGroupAccessList_String(t *testing.T) {
	list := GroupAccessList{
		{Group: Group{Path: "developers"}, Role: Access(gitlab.MaintainerPermissions)},
		{Group: Group{Path: "admins"}, Role: Access(gitlab.OwnerPermissions)},
	}
	expected := "[developers (Maintainer), admins (Owner)]"
	assert.Equal(t, expected, list.String(), "GroupAccessList String method should format list correctly")
}
func TestProjectAccess_String(t *testing.T) {
	perm := &ProjectAccess{
		Project: &Project{Path: "backend"},
		Role:    Access(gitlab.DeveloperPermissions),
	}
	expected := "backend (Developer)"
	assert.Equal(t, expected, perm.String(), "ProjectAccess String method should format correctly")
}

func TestProjectAccessList_String(t *testing.T) {
	list := ProjectAccessList{
		{Project: &Project{Path: "backend"}, Role: Access(gitlab.DeveloperPermissions)},
		{Project: &Project{Path: "frontend"}, Role: Access(gitlab.MaintainerPermissions)},
	}
	expected := "[backend (Developer), frontend (Maintainer)]"
	assert.Equal(t, expected, list.String(), "ProjectAccessList String method should format list correctly")
}

func TestGroups_Has(t *testing.T) {
	tests := []struct {
		name      string
		groups    Groups
		gid       int
		want      bool
		logOutput string
	}{
		{
			name: "Valid group ID",
			groups: Groups{
				1: Group{Name: "Group1"},
				2: Group{Name: "Group2"},
			},
			gid:  1,
			want: true,
		},
		{
			name: "Invalid group ID",
			groups: Groups{
				1: Group{Name: "Group1"},
				2: Group{Name: "Group2"},
			},
			gid:       3,
			want:      false,
			logOutput: "WARNING: group id 3 is not valid\n",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Capture log output
			var logBuffer bytes.Buffer

			log.SetOutput(&logBuffer)

			got := tt.groups.Has(tt.gid)
			if got != tt.want {
				t.Errorf("Has() = %v, want %v", got, tt.want)
			}

			// Check log output
			if tt.logOutput != "" && !strings.Contains(logBuffer.String(), tt.logOutput) {
				t.Errorf("log output = %v, want %v", logBuffer.String(), tt.logOutput)
			}

			// Reset log output
			log.SetOutput(os.Stderr)
		})
	}
}

func TestAccess_MarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		access   Access
		expected string
	}{
		{
			name:     "Admin",
			access:   Access(gitlab.AdminPermissions),
			expected: `"Admin"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buffer, err := tt.access.MarshalJSON()
			if err != nil {
				t.Errorf("MarshalJSON() error = %v", err)

				return
			}

			if string(buffer) != tt.expected {
				t.Errorf("MarshalJSON() got = %v, want %v", string(buffer), tt.expected)
			}
		})
	}
}

func TestNewGroup(t *testing.T) {
	tests := []struct {
		id       int
		name     string
		path     string
		expected Group
	}{
		{
			id:   1,
			name: "Group One",
			path: "group-one",
			expected: Group{
				ID:   1,
				Name: "Group One",
				Path: "group-one",
			},
		},
		{
			id:   2,
			name: "Group Two",
			path: "group-two",
			expected: Group{
				ID:   2,
				Name: "Group Two",
				Path: "group-two",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewGroup(tt.id, tt.name, tt.path)
			if got != tt.expected {
				t.Errorf("NewGroup() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestNewProject(t *testing.T) {
	tests := []struct {
		id       int
		name     string
		path     string
		expected *Project
	}{
		{
			id:   1,
			name: "Project One",
			path: "project-one",
			expected: &Project{
				ID:   1,
				Name: "Project One",
				Path: "project-one",
			},
		},
		{
			id:   2,
			name: "Project Two",
			path: "project-two",
			expected: &Project{
				ID:   2,
				Name: "Project Two",
				Path: "project-two",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewProject(tt.id, tt.name, tt.path)
			if *got != *tt.expected {
				t.Errorf("NewProject() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestNewUser(t *testing.T) {
	tests := []struct {
		id        int
		name      string
		username  string
		avatarURL string
		expected  *User
	}{
		{
			id:        1,
			name:      "User One",
			username:  "userone",
			avatarURL: "http://example.com/avatar1.png",
			expected: &User{
				ID:        1,
				Name:      "User One",
				Username:  "userone",
				AvatarURL: "http://example.com/avatar1.png",
				Groups:    make(GroupAccessList, 0),
				Projects:  make(ProjectAccessList, 0),
			},
		},
		{
			id:        2,
			name:      "User Two",
			username:  "usertwo",
			avatarURL: "http://example.com/avatar2.png",
			expected: &User{
				ID:        2,
				Name:      "User Two",
				Username:  "usertwo",
				AvatarURL: "http://example.com/avatar2.png",
				Groups:    make(GroupAccessList, 0),
				Projects:  make(ProjectAccessList, 0),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.username, func(t *testing.T) {
			got := NewUser(tt.id, tt.name, tt.username, tt.avatarURL)
			assert.Equal(t, tt.expected, got, "NewUser() = %v, want %v", got, tt.expected)
		})
	}
}

func TestSetGroupAccess(t *testing.T) {
	tests := []struct {
		name         string
		initial      *User
		groups       []*Group
		accessLevel  Access
		expected     GroupAccessList
		expectedLogs []string
	}{
		{
			name: "Add new group access",
			initial: &User{
				Groups: GroupAccessList{},
			},
			groups:      []*Group{{ID: 1, Name: "Group1", Path: "group1"}},
			accessLevel: Access(50),
			expected: GroupAccessList{
				{Group: Group{ID: 1, Name: "Group1", Path: "group1"}, Role: Access(50)},
			},
		},
		{
			name: "Groups are sorted",
			initial: &User{
				Groups: GroupAccessList{},
			},
			groups: []*Group{
				{ID: 3, Name: "C", Path: "C"},
				{ID: 2, Name: "B", Path: "B"},
				{ID: 1, Name: "A", Path: "A"},
			},
			accessLevel: Access(50),
			expected: GroupAccessList{
				{Group: Group{ID: 1, Name: "A", Path: "A"}, Role: Access(50)},
				{Group: Group{ID: 2, Name: "B", Path: "B"}, Role: Access(50)},
				{Group: Group{ID: 3, Name: "C", Path: "C"}, Role: Access(50)},
			},
		},
		{
			name: "Group access already exists",
			initial: &User{
				Groups: GroupAccessList{
					{Group: Group{ID: 1, Name: "Group1", Path: "group1"}, Role: Access(50)},
				},
			},
			groups:      []*Group{{ID: 1, Name: "Group1", Path: "group1"}},
			accessLevel: Access(40),
			expected: GroupAccessList{
				{Group: Group{ID: 1, Name: "Group1", Path: "group1"}, Role: Access(50)},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, g := range tt.groups {
				tt.initial.SetGroupAccess(*g, tt.accessLevel)
			}

			assert.Equal(t, tt.expected, tt.initial.Groups)
		})
	}
}

func TestSetProjectAccess(t *testing.T) {
	tests := []struct {
		name         string
		initial      *User
		projects     []*Project
		accessLevel  Access
		expected     ProjectAccessList
		expectedLogs []string
	}{
		{
			name: "Add new project access",
			initial: &User{
				Projects: ProjectAccessList{},
			},
			projects:    []*Project{NewProject(1, "Project1", "project1")},
			accessLevel: Access(50),
			expected: ProjectAccessList{
				{Project: NewProject(1, "Project1", "project1"), Role: Access(50)},
			},
		},
		{
			name: "Project access are sorted",
			initial: &User{
				Projects: ProjectAccessList{},
			},
			projects: []*Project{
				NewProject(4, "D", "projectD"),
				NewProject(2, "B", "projectB"),
				NewProject(1, "A", "projectA"),
				NewProject(3, "C", "projectC"),
			},
			accessLevel: Access(50),
			expected: ProjectAccessList{
				{Project: NewProject(1, "A", "projectA"), Role: Access(50)},
				{Project: NewProject(2, "B", "projectB"), Role: Access(50)},
				{Project: NewProject(3, "C", "projectC"), Role: Access(50)},
				{Project: NewProject(4, "D", "projectD"), Role: Access(50)},
			},
		},
		{
			name: "Project access already exists",
			initial: &User{
				Projects: ProjectAccessList{
					{Project: NewProject(1, "Project1", "project1"), Role: Access(50)},
				},
			},
			projects:    []*Project{NewProject(1, "Project1", "project1")},
			accessLevel: Access(40),
			expected: ProjectAccessList{
				{Project: NewProject(1, "Project1", "project1"), Role: Access(50)},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, p := range tt.projects {
				tt.initial.SetProjectAccess(p, tt.accessLevel)
			}

			assert.Equal(t, tt.expected, tt.initial.Projects)
		})
	}
}
