package domain

import (
	"testing"

	"github.com/xanzy/go-gitlab"
)

func TestAccessLevelToString(t *testing.T) {
	tests := []struct {
		name     string
		level    Access
		expected string
	}{
		{
			name:     "Test No Permissions",
			level:    Access(gitlab.NoPermissions),
			expected: "None",
		},
		{
			name:     "Test Minimal Permissions",
			level:    Access(gitlab.MinimalAccessPermissions),
			expected: "Minimal",
		},
		{
			name:     "Test Guest Permissions",
			level:    Access(gitlab.GuestPermissions),
			expected: "Guest",
		},
		{
			name:     "Test Reporter Permissions",
			level:    Access(gitlab.ReporterPermissions),
			expected: "Reporter",
		},
		{
			name:     "Test Developer Permissions",
			level:    Access(gitlab.DeveloperPermissions),
			expected: "Developer",
		},
		{
			name:     "Test Maintainer Permissions",
			level:    Access(gitlab.MaintainerPermissions),
			expected: "Maintainer",
		},
		{
			name:     "Test Owner Permissions",
			level:    Access(gitlab.OwnerPermissions),
			expected: "Owner",
		},
		{
			name:     "Test Admin Permissions",
			level:    Access(gitlab.AdminPermissions),
			expected: "Admin",
		},
		{
			name:     "Test Undefined Role",
			level:    9999,
			expected: "Custom Role 9999",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := AccessLevelToString(tt.level)
			if result != tt.expected {
				t.Errorf("AccessLevelToString(%d) = %s; want %s", tt.level, result, tt.expected)
			}
		})
	}
}
