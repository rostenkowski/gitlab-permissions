package domain

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

// GroupsService interface encapsulates methods used to interact with GitLab groups.
type GroupsService interface {
	GetGroup(gid interface{}, opt *gitlab.GetGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Group, *gitlab.Response, error)

	// ListGroupMembers lists members of a group.
	ListGroupMembers(gid interface{}, opt *gitlab.ListGroupMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.GroupMember, *gitlab.Response, error)

	// ListGroupProjects lists projects in a group.
	ListGroupProjects(gid interface{}, opt *gitlab.ListGroupProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error)

	// ListSubGroups lists subgroups of a group.
	ListSubGroups(gid interface{}, opt *gitlab.ListSubGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error)
}

// ProjectMembersService defines methods for managing project members in GitLab.
type ProjectMembersService interface {
	// ListProjectMembers lists members of a project.
	ListProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error)

	// ListAllProjectMembers lists all members of a project.
	ListAllProjectMembers(pid interface{}, opt *gitlab.ListProjectMembersOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectMember, *gitlab.Response, error)
}

// UsersService interface encapsulates methods used to interact with GitLab users.
type UsersService interface {
	// GetUser fetches a user by ID.
	GetUser(uid int, opt gitlab.GetUsersOptions, options ...gitlab.RequestOptionFunc) (*gitlab.User, *gitlab.Response, error)
}

// ProjectsService interface encapsulates methods used to interact with GitLab projects.
type ProjectsService interface {
	// GetProject fetches a project by ID.
	GetProject(pid interface{}, opt *gitlab.GetProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Project, *gitlab.Response, error)
}

// AccessLevelToString converts access level value to string description.
func AccessLevelToString(level Access) string {
	switch gitlab.AccessLevelValue(level) {
	case gitlab.NoPermissions:
		return "None"
	case gitlab.MinimalAccessPermissions:
		return "Minimal"
	case gitlab.GuestPermissions:
		return "Guest"
	case gitlab.ReporterPermissions:
		return "Reporter"
	case gitlab.DeveloperPermissions:
		return "Developer"
	case gitlab.MaintainerPermissions:
		return "Maintainer"
	case gitlab.OwnerPermissions:
		return "Owner"
	case gitlab.AdminPermissions:
		return "Admin"
	default:
		return fmt.Sprintf("Custom Role %d", level)
	}
}
