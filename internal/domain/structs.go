//nolint:tagliatelle
package domain

import (
	"encoding/json"
	"fmt"
	"log"
	"sort"

	"github.com/xanzy/go-gitlab"
)

type Collection []*User

type Projects map[int]*gitlab.Project

type Project struct {
	ID   int    `json:"-"`
	Name string `json:"-"`
	Path string `json:"Path"`
}

type Groups map[int]Group

type Group struct {
	ID   int    `json:"-"`
	Name string `json:"-"`
	Path string `json:"Path"`
}

// Access is a custom type that wraps an int value.
type Access gitlab.AccessLevelValue

// MarshalJSON implements the json.Marshaler interface.
// It converts the Access value to a string representation for JSON encoding purposes.
func (a Access) MarshalJSON() ([]byte, error) {
	return json.Marshal(AccessLevelToString(a))
}

func NewGroup(id int, name, path string) Group {
	return Group{
		ID:   id,
		Name: name,
		Path: path,
	}
}

func NewProject(id int, name, path string) *Project {
	return &Project{
		ID:   id,
		Name: name,
		Path: path,
	}
}

type Users map[int]*User

// NewUser construct new User from the provided parameters.
func NewUser(id int, name, username, avatarURL string) *User {
	return &User{
		ID:        id,
		Name:      name,
		Username:  username,
		AvatarURL: avatarURL,
		Groups:    GroupAccessList{},
		Projects:  ProjectAccessList{},
	}
}

// User represents a user in the system.
type User struct {
	ID        int               `json:"-"`
	Name      string            `json:"Name"`
	Username  string            `json:"Username"`
	AvatarURL string            `json:"-"`
	Groups    GroupAccessList   `json:"Groups"`
	Projects  ProjectAccessList `json:"Projects"`
}

// SetGroupAccess looks for an existing group access and creates one when nothing is found. Warns otherwise.
func (u *User) SetGroupAccess(group Group, accessLevel Access) {
	for _, x := range u.Groups {
		if x.Group.ID == group.ID {
			log.Printf("user: %s - access to group %d already exist with %s, ignoring %s\n", u.Username, group.ID, AccessLevelToString(x.Role), AccessLevelToString(accessLevel))

			return
		}
	}

	u.Groups = append(u.Groups, GroupAccess{
		Group: group,
		Role:  accessLevel,
	})

	// Sort the group permissions by group.Path
	sort.Slice(u.Groups, func(i, j int) bool {
		return u.Groups[i].Group.Path < u.Groups[j].Group.Path
	})
}

// SetProjectAccess looks for an existing project access and creates one when nothing is found. Warns otherwise.
func (u *User) SetProjectAccess(project *Project, accessLevel Access) {
	for _, x := range u.Projects {
		if x.Project.ID == project.ID {
			log.Printf("user: %s - access to project %s already exist with %s, ignoring %s\n", u.Name, project.Path, AccessLevelToString(x.Role), AccessLevelToString(accessLevel))

			return
		}
	}

	u.Projects = append(u.Projects, ProjectAccess{
		Project: project,
		Role:    accessLevel,
	})

	// Sort the project permissions by project.Path
	sort.Slice(u.Projects, func(i, j int) bool {
		return u.Projects[i].Project.Path < u.Projects[j].Project.Path
	})
}

type GroupAccess struct {
	Group Group
	Role  Access
}

func (p *GroupAccess) String() string {
	return fmt.Sprintf("%s (%s)", p.Group.Path, AccessLevelToString(p.Role))
}

type ProjectAccess struct {
	Project *Project
	Role    Access
}

func (g Groups) Has(gid int) bool {
	if _, ok := g[gid]; !ok {
		log.Printf("WARNING: group id %d is not valid\n", gid)

		return false
	}

	return true
}

type GroupAccessList []GroupAccess

func (l GroupAccessList) String() string {
	var result string

	j := 0
	for _, p := range l {
		if j == 0 {
			j++
			result = p.String()
		} else {
			result = result + ", " + p.String()
		}
	}

	return "[" + result + "]"
}

func (p *ProjectAccess) String() string {
	return fmt.Sprintf("%s (%s)", p.Project.Path, AccessLevelToString(p.Role))
}

type ProjectAccessList []ProjectAccess

// String returns a string representation of the list of project permissions.
func (l ProjectAccessList) String() string {
	var result string

	j := 0

	for _, p := range l {
		if j == 0 {
			j++
			result = p.String()
		} else {
			result = result + ", " + p.String()
		}
	}

	return "[" + result + "]"
}
