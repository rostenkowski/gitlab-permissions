package filesystem

import (
	"errors"
	"os"
	"testing"
)

type mockDirEntry struct {
	name  string
	isDir bool
}

func (m mockDirEntry) Name() string               { return m.name }
func (m mockDirEntry) IsDir() bool                { return m.isDir }
func (m mockDirEntry) Type() os.FileMode          { return 0 }
func (m mockDirEntry) Info() (os.FileInfo, error) { return os.FileInfo(nil), nil }

func TestDeleteFilesInDirectory(t *testing.T) {
	tests := []struct {
		name       string
		setupFiles []os.DirEntry
		readDirErr error
		removeSuc  bool
		expectErr  bool
	}{
		{
			name:       "NoFiles",
			setupFiles: []os.DirEntry{},
			readDirErr: nil,
			removeSuc:  true,
			expectErr:  false,
		},
		{
			name: "MultipleFiles",
			setupFiles: []os.DirEntry{
				mockDirEntry{name: "file1.txt"},
				mockDirEntry{name: "file2.txt"},
				mockDirEntry{name: "file3.txt"},
			},
			readDirErr: nil,
			removeSuc:  true,
			expectErr:  false,
		},
		{
			name: "WithSubdirectory",
			setupFiles: []os.DirEntry{
				mockDirEntry{name: "file1.txt"},
				mockDirEntry{name: "subdir", isDir: true},
			},
			readDirErr: nil,
			removeSuc:  true,
			expectErr:  false,
		},
		{
			name:       "ReadDirFailure",
			setupFiles: nil,
			readDirErr: errors.New("mock read dir error"),
			removeSuc:  true,
			expectErr:  true,
		},
		{
			name: "RemoveFailure",
			setupFiles: []os.DirEntry{
				mockDirEntry{name: "file1.txt"},
			},
			readDirErr: nil,
			removeSuc:  false,
			expectErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Mock readDir
			readDir = func(_ string) ([]os.DirEntry, error) {
				return tt.setupFiles, tt.readDirErr
			}
			// Mock remover
			remove = func(_ string) error {
				if tt.removeSuc {
					return nil
				}

				return errors.New("mock error")
			}

			err := Purge("testdir")
			if (err != nil) != tt.expectErr {
				t.Errorf("DeleteFilesInDirectory() error = %v, expectErr %v", err, tt.expectErr)
			}
		})
	}
}
