package filesystem

import (
	"log"
	"os"
	"path/filepath"

	"gitlab.com/rostenkowski/gitlab-permissions/internal/collect"
)

// Define variables for file operations to allow dependency injection.
var (
	CheckDirFunc = CheckDir
	mkdirDoer    = os.MkdirAll
	createFile   = os.Create
	openFile     = os.OpenFile
	stat         = os.Stat
)

// CheckDir checks if a directory exists and is writable, or if it can be created.
func CheckDir(path string) bool {
	// Check if directory exists
	info, err := stat(path)
	if err == nil {
		if !info.IsDir() {
			return false
		}

		// Try to create a temporary file in the directory to check writability
		tmpFilePath := filepath.Join(path, ".tmp-check")
		testMode := 0o600
		tmpFile, err := openFile(tmpFilePath, os.O_WRONLY|os.O_CREATE|os.O_EXCL, os.FileMode(testMode))

		if err != nil {
			return false
		}

		_ = tmpFile.Close()
		_ = os.Remove(tmpFilePath) // Clean up after the check

		return true
	}

	if !os.IsNotExist(err) {
		log.Printf("cache directory check failed: %s\n", err)

		return false
	}

	// Directory does not exist, try to create it
	if err := mkdirDoer(path, collect.CacheDirectoryMode); err != nil {
		log.Printf("failed to create directory: %s\n", err)

		return false
	}

	// Directory created successfully
	return true
}
