package filesystem

import (
	"errors"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

//lint:ignore funlen reason="This test function needs to perform multiple checks"
func TestCheckDir(t *testing.T) {
	// Save the original functions to restore later
	originalMkdirDoer := mkdirDoer
	originalCreateFile := createFile
	originalOpenFile := openFile
	originalStat := stat

	defer func() {
		mkdirDoer = originalMkdirDoer
		createFile = originalCreateFile
		openFile = originalOpenFile
		stat = originalStat
	}()

	tests := []struct {
		name     string
		setup    func() (string, func()) // setup function returns the path and a cleanup function
		expected bool
		patch    func() (func(), func()) // function to patch and reset the operations
	}{
		{
			name: "Directory exists and writable",
			setup: func() (string, func()) {
				dir := t.TempDir() // Creates a temporary directory that will be cleaned up automatically

				return dir, func() {}
			},
			expected: true,
			patch:    func() (func(), func()) { return func() {}, func() {} }, // No patching needed
		},
		{
			name: "Directory does not exist and created successfully",
			setup: func() (string, func()) {
				dir := filepath.Join(os.TempDir(), "nonexistent-dir")

				return dir, func() { _ = os.RemoveAll(dir) } // Clean up after test
			},
			expected: true,
			patch:    func() (func(), func()) { return func() {}, func() {} }, // No patching needed
		},
		{
			name: "Path exists but is not a directory",
			setup: func() (string, func()) {
				file, err := os.CreateTemp("", "file-*")
				if err != nil {
					t.Fatal(err)
				}
				_ = file.Close()

				return file.Name(), func() { _ = os.Remove(file.Name()) } // Clean up after test
			},
			expected: false,
			patch:    func() (func(), func()) { return func() {}, func() {} }, // No patching needed
		},
		{
			name: "Directory exists but is not writable",
			setup: func() (string, func()) {
				dir := t.TempDir() // Create a temporary directory

				return dir, func() { _ = os.RemoveAll(dir) } // Clean up after test
			},
			expected: false,
			patch: func() (func(), func()) {
				originalOpenFile := openFile
				openFile = func(_ string, _ int, _ os.FileMode) (*os.File, error) {
					return nil, errors.New("simulated open file failure")
				}

				return func() {}, func() { openFile = originalOpenFile }
			},
		},
		{
			name: "Failed to check directory due to other error",
			setup: func() (string, func()) {
				return filepath.Join(os.TempDir(), "some-dir"), func() {}
			},
			expected: false,
			patch: func() (func(), func()) {
				originalStat := stat
				stat = func(_ string) (os.FileInfo, error) {
					return nil, errors.New("simulated stat error")
				}

				return func() {}, func() { stat = originalStat }
			},
		},
		{
			name: "Failed to create directory using mocked mkdirDoer",
			setup: func() (string, func()) {
				// Use a path that should fail due to the patched mkdirDoer
				dir := filepath.Join(os.TempDir(), "mocked-failure-dir")

				return dir, func() { _ = os.RemoveAll(dir) } // Clean up after test
			},
			expected: false,
			patch: func() (func(), func()) {
				originalMkdirDoer := mkdirDoer
				mkdirDoer = func(_ string, _ os.FileMode) error {
					return errors.New("simulated mkdir failure")
				}

				return func() {}, func() { mkdirDoer = originalMkdirDoer }
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Apply the patch and get the reset function
			_, reset := tt.patch()

			defer reset()

			path, cleanup := tt.setup()

			defer cleanup()

			result := CheckDir(path)
			assert.Equal(t, tt.expected, result)
		})
	}
}
