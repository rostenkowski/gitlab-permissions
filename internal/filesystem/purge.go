package filesystem

import (
	"fmt"
	"os"
	"path/filepath"
)

var (
	remove  = os.Remove
	readDir = os.ReadDir
)

// Purge deletes all files in the specified directory.
func Purge(dir string) error {
	// List files
	files, err := readDir(dir)
	if err != nil {
		return fmt.Errorf("failed to read directory: %w", err)
	}

	// Delete files
	for _, file := range files {
		if !file.IsDir() {
			filePath := filepath.Join(dir, file.Name())
			err = remove(filePath)

			if err != nil {
				return fmt.Errorf("failed to delete file %s: %w", filePath, err)
			}
		}
	}

	return nil
}
