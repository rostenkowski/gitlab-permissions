# gitlab-permissions

[![pipeline status](https://gitlab.com/rostenkowski/gitlab-permissions/badges/main/pipeline.svg)](https://gitlab.com/rostenkowski/gitlab-permissions/-/commits/main)
[![coverage report](https://gitlab.com/rostenkowski/gitlab-permissions/badges/main/coverage.svg)](https://gitlab.com/rostenkowski/gitlab-permissions/-/commits/main)

![GitLab Permissions Viewer](docs/theme.webp)

## Overview

This program provides insight into permission settings within a specified GitLab group. Designed with security,
performance, and reliability in mind, it is an essential tool for administrators and SRE teams to maintain an overview
of their GitLab environments.

The application operates by periodically fetching data, which it securely stores and serves through a robust HTTP
server. This server delivers fresh permission details rapidly and reliably, ensuring users have access to the latest
information with minimal delay while avoiding repeated calls to the GitLab API by using an HTTP response-caching
mechanism.

The HTTP server, with built-in SSL support, delivers permissions data with sub-millisecond response times, offering a
blazingly fast user experience while ensuring state-of-the-art security with bearer token authentication.

## Features

- **Periodic Data Fetching**: Periodically retrieves comprehensive details about groups, members, and projects from
  GitLab's API, ensuring users have access to the latest information with minimal delay.
- **HTTP Server with SSL Support**: Serves permission details through a robust HTTP server with built-in SSL support,
  providing sub-millisecond response times and ensuring secure data transmission with bearer token authentication.
- **Caching Mechanism**: Utilizes HTTP response-caching to reduce the frequency of API calls, lowering latency and
  enhancing response speed.
- **Secure Environment Configuration**: Uses environment variables for sensitive settings like the GitLab API access
  token and validates all configurations to ensure correct application startup.

## Installation

### 1. Source

Download the [source files](https://gitlab.com/rostenkowski/gitlab-permissions) from GitLab and build the binary:

Download using the Git protocol:

```shell
git clone git@gitlab.com:rostenkowski/gitlab-permissions.git
```

Download using HTTP protocol:

```shell
git clone https://gitlab.com/rostenkowski/gitlab-permissions.git
```

Build the binary:

```shell
go build -o gitlab-permissions main.go
```

Put the binary into your bin directory, preferably somewhere in $PATH:

```shell
mv gitlab-permissions /usr/local/bin/
```

### 2. Go

You can install the binary using the built-in Go tooling using the following command:

```shell
go install gitlab.com/rostenkowski/gitlab-permissions@v1.0.63
```

### 3. Docker

```shell
docker pull rostenkowski/gitlab-permissions:1.0.63
```

Configuration example running in Docker:

```shell
docker run --rm \
    -p 8080:8080 \
    -p 8443:8443 \
    -v ./cache:/cache \
    -v ./cert.pem:/cert.pem \
    -v ./key.pem:/key.pem \
    -e GITLAB_ACCESS_TOKEN="<GITLAB_ACCESS_TOKEN>" \
    -e GITLAB_GROUP_ID="<GITLAB_GROUP_ID>" \
    -e API_KEY="<SECRET_TOKEN>" \
    rostenkowski/gitlab-permissions:1.0.63 \
    gitlab-permissions -cacheTTL 300 -cacheDir /cache -ssl -cert /cert.pem -key /key.pem 
```


### 4. Download

Download latest binary from the [Releases](https://gitlab.com/rostenkowski/gitlab-permissions/-/releases) page.

## Configuration

Set up your GitLab settings:

```shell
export GITLAB_ACCESS_TOKEN="<YOUR_GITLAB_ACCESS_TOKEN>"
export GITLAB_GROUP_ID="<YOUR_GITLAB_GROUP_ID>"
```

### Authentication

Authentication is optional and can be enabled by exporting the `API_KEY` variable in the server environment. If the
variable is missing, the server does not require any authentication and only prints a warning to the log that the
authentication is disabled.

To enable authentication for the server (e.g., when running on a public-facing machine):

```shell
export API_KEY="<YOUR_SECRET_TOKEN>"
```

When authentication is enabled, clients are required to send the `Authorization` header with the bearer token.

## Usage

### Run the HTTP Server with default settings

```shell
gitlab-permissions
```

### Run with custom port, cache directory, and cache TTL

```shell
gitlab-permissions -port 8888 -cacheDir ./somewhere -cacheTTL 1800
```

### Run HTTPS server with SSL certificate

```shell
gitlab-permissions -port 8080 -ssl -sslPort 10433 -cert cert.pem -key key.pem
```

Note: All requests sent to the HTTP endpoint will be redirected to HTTPS if the application is running with SSL enabled.

### Requesting Data

For human-readable text format, use:

```shell
curl -s http://localhost:8080/
```

Example output:

```text
Howard Wolowitz (@howard)
Groups:    [big-bang-theory/universe (Developer)]
Projects:  [big-bang-theory/universe/earth (Owner), big-bang-theory/universe/moon (Maintainer)]

Sheldon Cooper (@sheldon)
Groups:    [big-bang-theory/universe (Developer)]
Projects:  [big-bang-theory/universe/earth (Maintainer), big-bang-theory/universe/moon (Owner)]

Total Users: 2
```

### Authentication

When authentication is enabled, add the "Authorization" header to the request:

```shell
curl -s -H 'Authorization: Bearer <YOUR_SECRET_TOKEN>' http://localhost:8080/
```

### Advanced Usage with JSON Format

This allows for tools like **jq** to process the JSON data:

```shell
curl -s http://localhost:8080/json
```

#### Example Commands

List all users with access to anything within the inspected group:

```shell
curl -s http://localhost:8080/json | jq ".[].Name"
```

Example output:

```text
"Howard Wolowitz"
"Sheldon Cooper"
```

List all groups within the inspected group somebody has granted access to:

```shell
curl -s localhost:8080/json | jq ".[].Groups.[].Group.Path" | sort | uniq
```

Example output:

```text
"big-bang-theory"
"big-bang-theory/universe"
```

List all projects within the inspected group somebody has granted access to:

Note: This does not list all projects in the namespace, just those that have direct members.

```shell
curl -s localhost:8080/json | jq ".[].Projects.[].Project.Path" | sort | uniq
```

Example output:

```text
"big-bang-theory/universe/earth"
"big-bang-theory/universe/moon"
```

## Contributing

Please feel free to file an Issue and/or open a Merge Request.

### Prerequisites

Make sure you have [installed](https://go.dev/doc/install) Go.
