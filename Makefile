.PHONY: vendor build lint clean tests run coverage report certificate

# Path to the Go binary
GO="$(shell which go)"

# Project variables
BINARY_NAME=gitlab-permissions
BUILD_DIR=./build
CACHE_DIR=./cache

# Fetches and installs dependencies locally
vendor:
	@$(GO) mod vendor

# Builds the project binary
build: vendor
	@$(GO) build -o $(BUILD_DIR)/$(BINARY_NAME) main.go

# Lints the code using golangci-lint
lint: vendor
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	golangci-lint run ./internal/...
	golangci-lint run ./main.go

# Cleans the build and cache directories, and removes test artifacts
clean:
	@if [ -z "$(BUILD_DIR)" ] || [ -z "$(CACHE_DIR)" ]; then \
		echo "BUILD_DIR or CACHE_DIR is undefined. Clean aborted to prevent system damage.";\
	else \
		rm -rf $(BUILD_DIR)/*;\
		rm -rf $(CACHE_DIR)/*;\
	fi
	@rm -f coverage.out coverage.html
	@rm -f cert.pem
	@rm -f key.pem

# Runs the test suite with race detection
tests: vendor
	@$(GO) test ./internal/... -v -race

# Runs the application, requiring specific environment variables
run: vendor
	@test -n "$$GITLAB_ACCESS_TOKEN" && test -n "$$GITLAB_GROUP_ID" || (echo "ERROR: GITLAB_ACCESS_TOKEN and GITLAB_GROUP_ID must be set" && exit 1)
	@GITLAB_ACCESS_TOKEN="$$GITLAB_ACCESS_TOKEN" GITLAB_GROUP_ID="$$GITLAB_GROUP_ID" $(GO) run -race main.go

# Generates a test coverage report
coverage: vendor
	@CGO_ENABLED=1 $(GO) test ./internal/... -v -race -covermode=atomic -coverprofile=coverage.out
	@$(GO) tool cover -func=coverage.out

# Opens the coverage report in a browser
report: coverage
	@$(GO) tool cover -html=coverage.out -o coverage.html
	@UNAME_S=`uname -s` ; \
	if [ "$$UNAME_S" = "Darwin" ]; then \
		open coverage.html ; \
	elif [ "$$UNAME_S" = "Linux" ]; then \
		xdg-open coverage.html ; \
	elif [ "$$UNAME_S" = "CYGWIN_NT-10.0" ] || [ "$$UNAME_S" = "MINGW32_NT-6.1" ] || [ "$$UNAME_S" = "MSYS_NT-10.0" ]; then \
		start coverage.html ; \
	else \
		echo "Platform $$UNAME_S not supported" ; \
	fi

# Generates a self-signed certificate for testing purposes
certificate:
	@openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key.pem -out cert.pem -subj "/C=US/ST=California/L=San Francisco/O=Example Company/OU=IT Department/CN=example.com/emailAddress=admin@example.com"
