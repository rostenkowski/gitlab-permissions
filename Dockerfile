# Stage 1: Build the Go binary
FROM golang:1.18-alpine AS builder

# Set the current working directory inside the container
WORKDIR /build

# Copy the dependencies files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the working directory inside the container
COPY . .

# Build the Go binary
RUN go build -o gitlab-permissions main.go

# Stage 2: Run the Go binary in a minimal Docker image
FROM alpine:latest

# Copy the pre-built binary file from the previous stage
COPY --from=builder /build/gitlab-permissions /usr/local/bin/gitlab-permissions

# Expose ports
EXPOSE 8080 8443

# Command to run
CMD ["./gitlab-permissions"]
